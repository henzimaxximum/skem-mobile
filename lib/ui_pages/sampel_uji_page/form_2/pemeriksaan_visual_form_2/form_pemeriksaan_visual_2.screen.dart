import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:skem/layouts/custom_widget/form/inspeksi_form_card.dart';
import 'package:skem/models/form_item_model.dart';
import 'package:skem/ui_pages/sampel_uji_page/form_2/pemeriksaan_visual_form_2/pemeriksaan_visual_2_questions.dart';

import '../../../../layouts/custom_widget/buttons/filled_button_custom.dart';
import '../../../../layouts/custom_widget/form/dropdown_custom.dart';
import '../../../../layouts/custom_widget/form/dropdown_item_custom.dart';
import '../../../../layouts/custom_widget/form/inspeksi_input_custom.dart';
import '../../../../layouts/custom_widget/page_layout/blue_linear_background.dart';
import '../../../../shared/shared_theme/fonts.dart';

class FormPemeriksaanVisual2Screen extends StatefulWidget {
  const FormPemeriksaanVisual2Screen({super.key});

  @override
  State<FormPemeriksaanVisual2Screen> createState() => _FormPemeriksaanVisual2ScreenState();
}

class _FormPemeriksaanVisual2ScreenState extends State<FormPemeriksaanVisual2Screen> {
  FormItemModel? selectedSampelSesuai;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.white,
        elevation: 0.6,
        toolbarHeight: 70,

        // Icon Arrow Back
        leading: IconButton(
          onPressed: (){
            Navigator.pop(context);
          },

          icon: const Icon(
            Icons.arrow_back,
            size: 26,
            color: Colors.black,
          ),
        ),

        // Page Title
        title: Padding(
          padding: EdgeInsets.only(right: 20),
          child: Text(
            'Pemeriksaan Visual + Entri Data Ketidaksesuaian',
            textAlign: TextAlign.center,
            maxLines: 2,
            style: blackTextStyle.copyWith(
              fontSize: 18,
              fontWeight: bold,
            ),
          ),
        ),
      ),

      body: BlueLinearBackground(
        middleStop: 0.2,
        child: Stack(
          children: [
            // Icon Kuning Kiri Atas
            Positioned(
              top: -25,
              left: -55,

              child: SvgPicture.asset(
                'assets/icon/svg_circle_kiri_atas.svg',
                width: 220,
                height: 220,
              ),
            ),

            // Icon Kuning Kanan Bawah
            Positioned(
              bottom: -25,
              right: -55,

              child: SvgPicture.asset(
                'assets/icon/svg_circle_kanan_bawah.svg',
                width: 220,
                height: 220,
              ),
            ),

            // Main Page Content
            ListView(
              padding: const EdgeInsets.all(20),
              children: [
                const SizedBox(height: 10),

                // Section Form Detail Ritel Baru
                formDetailRitelBaru(),

                const SizedBox(height: 25),

                // Batalkan & Lanjutkan Button
                bottomButtons(context),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget formDetailRitelBaru(){
    return InspeksiFormCard(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // Nomor SHE
          const InspeksiInputCustom(
            formLabel: 'Nomor SHE',
            hintText: 'Isikan nomor SHE',
          ),

          const SizedBox(height: 16),

          // Jumlah Bintang
          const InspeksiInputCustom(
            formLabel: 'Jumlah Bintang',
            hintText: 'Isikan jumlah bintang',
          ),

          const SizedBox(height: 16),

          // Nilai Efisiensi Energi (EER), CSPF
          const InspeksiInputCustom(
            formLabel: 'Nilai Efisiensi Energi (EER/CSPF)',
            hintText: 'Isikan nilai EER, CSPF',
          ),

          const SizedBox(height: 16),

          // Model Unit Dalam
          const InspeksiInputCustom(
            formLabel: 'Model Unit Dalam',
            hintText: 'Isikan model unit dalam',
          ),

          const SizedBox(height: 16),

          // Model Unit Luar
          const InspeksiInputCustom(
            formLabel: 'Model Unit Luar',
            hintText: 'Isikan model unit luar',
          ),

          const SizedBox(height: 16),

          // Daya
          const InspeksiInputCustom(
            formLabel: 'Daya',
            hintText: 'Isikan daya',
          ),

          const SizedBox(height: 16),

          // Kapasitas Pendinginan
          const InspeksiInputCustom(
            formLabel: 'Kapasitas Pendinginan',
            hintText: 'Isikan kapasitas pendinginan',
          ),

          const SizedBox(height: 24),

          // Apakah informasi sampel sudah sesuai
          sampelSesuaiDropdown(),
        ],
      ),
    );
  }

  Widget sampelSesuaiDropdown(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // Text Tipe Lokasi Pengawasan
        Text(
          'Apakah informasi sampel sudah sesuai?',
          style: blackTextStyle.copyWith(
            fontSize: 13,
            fontWeight: semiBold,
          ),
        ),

        const SizedBox(height: 16),

        DropdownCustom(
          dropdownText: selectedSampelSesuai?.text,
          child: Column(
            children: listSampelSesuai.map((itemLokasi){
              return GestureDetector(
                onTap: (){
                  selectedSampelSesuai = itemLokasi;
                  setState(() {});
                },

                child: DropdownItemCustom(
                  formItem: itemLokasi,
                  isItemSelected: selectedSampelSesuai?.id == itemLokasi.id,
                ),
              );
            }).toList(),
          ),
        )
      ],
    );
  }

  Widget bottomButtons(BuildContext context){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        // Batalkan
        FilledButtonCustom(
          width: MediaQuery.sizeOf(context).width * 0.38,
          height: 56,
          buttonColor: const Color(0xffEB5757),
          label: Text(
            'Kembali',
            style: whiteTextStyle.copyWith(
                fontSize: 16,
                fontWeight: bold
            ),
          ),

          onTap: (){
            Navigator.pop(context);
          },
        ),

        // Batalkan
        FilledButtonCustom(
          width: MediaQuery.sizeOf(context).width * 0.45,
          height: 56,
          buttonColor: const Color(0xff04AFD2),
          label: Text(
            'Lanjutkan',
            style: whiteTextStyle.copyWith(
                fontSize: 16,
                fontWeight: bold
            ),
          ),

          onTap: (){
            Navigator.pushNamed(context, '/form-cek-fisik-indoor-2');
          },
        ),
      ],
    );
  }
}
