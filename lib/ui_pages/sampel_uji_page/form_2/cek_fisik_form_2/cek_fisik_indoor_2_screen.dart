import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';
import 'package:skem/layouts/custom_widget/form/inspeksi_form_card.dart';

import '../../../../layouts/custom_widget/buttons/filled_button_custom.dart';
import '../../../../layouts/custom_widget/form/checkbox_form_custom.dart';
import '../../../../layouts/custom_widget/page_layout/blue_linear_background.dart';
import '../../../../shared/shared_theme/fonts.dart';

class FormCekFisikIndoor2Screen extends StatefulWidget {
  const FormCekFisikIndoor2Screen({super.key});

  @override
  State<FormCekFisikIndoor2Screen> createState() => _FormCekFisikIndoor2ScreenState();
}

class _FormCekFisikIndoor2ScreenState extends State<FormCekFisikIndoor2Screen> {
  String? _selectedKondisiKemasanAC;
  String? _selectedKondisiFisikAC;
  String? _selectedKondisiKipasUnitIndoor;
  String? _selectedKomponenHilang;
  String? _selectedpenutupKatub;
  String? _selectedPipaPenghubung;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.white,
        elevation: 0.5,
        toolbarHeight: 60,

        // Icon Arrow Back
        leading: IconButton(
          onPressed: (){
            Navigator.pop(context);
          },

          icon: const Icon(
            Icons.arrow_back,
            size: 26,
            color: Colors.black,
          ),
        ),

        // Page Title
        title: Text(
          'Cek Fisik: Unit Indoor',
          textAlign: TextAlign.center,
          maxLines: 2,
          style: blackTextStyle.copyWith(
            fontSize: 18,
            fontWeight: bold,
          ),
        ),
      ),

      body: BlueLinearBackground(
        middleStop: 0.2,
        child: Stack(
          children: [
            // Icon Kuning Kiri Atas
            Positioned(
              top: -25,
              left: -55,

              child: SvgPicture.asset(
                'assets/icon/svg_circle_kanan_bawah.svg',
                width: 220,
                height: 220,
              ),
            ),

            // Icon Kuning Kanan Bawah
            Positioned(
              bottom: -25,
              right: -55,

              child: SvgPicture.asset(
                'assets/icon/svg_circle_kanan_bawah.svg',
                width: 220,
                height: 220,
              ),
            ),

            // Main Page Content
            ListView(
              padding: const EdgeInsets.all(20),
              children: [
                formCekFisikIndoor(),

                const SizedBox(height: 25),

                // Batalkan & Lanjutkan Button
                bottomButtons(context),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget formCekFisikIndoor(){
    return InspeksiFormCard(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // Kondisi kemasan produk AC tidak rusak
          CheckboxFormCustom(
            textLabel: 'Kondisi kemasan produk AC tidak rusak',
            listCheckbox: Row(
              children: [
                Expanded(
                  child: CustomCheckbox(
                    value: 'Baik',
                    groupValue: _selectedKondisiKemasanAC,
                    onChanged: (String? value){
                      setState(() {
                        _selectedKondisiKemasanAC = value;
                      });
                    },
                  ),
                ),

                Expanded(
                  child: CustomCheckbox(
                    value: 'Tidak Baik',
                    groupValue: _selectedKondisiKemasanAC,
                    onChanged: (String? value){
                      setState(() {
                        _selectedKondisiKemasanAC = value;
                      });
                    },
                  ),
                )
              ],
            ),
          ),

          const SizedBox(height: 28),

          // Kondisi fisik produk AC secara visual seperti bodi unit indoor
          CheckboxFormCustom(
            textLabel: 'Kondisi fisik produk AC secara visual seperti bodi unit indoor',
            listCheckbox: Row(
              children: [
                Expanded(
                  child: CustomCheckbox(
                    value: 'Baik',
                    groupValue: _selectedKondisiFisikAC,
                    onChanged: (String? value){
                      setState(() {
                        _selectedKondisiFisikAC = value;
                      });
                    },
                  ),
                ),

                Expanded(
                  child: CustomCheckbox(
                    value: 'Tidak Baik',
                    groupValue: _selectedKondisiFisikAC,
                    onChanged: (String? value){
                      setState(() {
                        _selectedKondisiFisikAC = value;
                      });
                    },
                  ),
                )
              ],
            ),
          ),

          const SizedBox(height: 28),

          // Kondisi kipas pada unit indoor tidak retak, rusak, atau menyentuh bagian lainnya
          CheckboxFormCustom(
            textLabel: 'Kondisi kipas pada unit indoor tidak retak, rusak, atau menyentuh bagian lainnya',
            listCheckbox: Row(
              children: [
                Expanded(
                  child: CustomCheckbox(
                    value: 'Baik',
                    groupValue: _selectedKondisiKipasUnitIndoor,
                    onChanged: (String? value){
                      setState(() {
                        _selectedKondisiKipasUnitIndoor = value;
                      });
                    },
                  ),
                ),

                Expanded(
                  child: CustomCheckbox(
                    value: 'Tidak Baik',
                    groupValue: _selectedKondisiKipasUnitIndoor,
                    onChanged: (String? value){
                      setState(() {
                        _selectedKondisiKipasUnitIndoor = value;
                      });
                    },
                  ),
                )
              ],
            ),
          ),

          const SizedBox(height: 28),

          // Tidak ada komponen aksesoris yang hilang atau tidak lengkap
          CheckboxFormCustom(
            textLabel: 'Tidak ada komponen aksesoris yang hilang atau tidak lengkap (screw, remote control, jolder, dll)',
            listCheckbox: Row(
              children: [
                Expanded(
                  child: CustomCheckbox(
                    value: 'Baik',
                    groupValue: _selectedKomponenHilang,
                    onChanged: (String? value){
                      setState(() {
                        _selectedKomponenHilang = value;
                      });
                    },
                  ),
                ),

                Expanded(
                  child: CustomCheckbox(
                    value: 'Tidak Baik',
                    groupValue: _selectedKomponenHilang,
                    onChanged: (String? value){
                      setState(() {
                        _selectedKomponenHilang = value;
                      });
                    },
                  ),
                )
              ],
            ),
          ),

          const SizedBox(height: 28),

          // Terdapat penutup katub atau tidak ada kerusakan pada ulir sambungan pipa antara unit indoor
          CheckboxFormCustom(
            textLabel: 'Terdapat penutup katub atau tidak ada kerusakan pada ulir sambungan pipa antara unit indoor',
            listCheckbox: Row(
              children: [
                Expanded(
                  child: CustomCheckbox(
                    value: 'Baik',
                    groupValue: _selectedpenutupKatub,
                    onChanged: (String? value){
                      setState(() {
                        _selectedpenutupKatub = value;
                      });
                    },
                  ),
                ),

                Expanded(
                  child: CustomCheckbox(
                    value: 'Tidak Baik',
                    groupValue: _selectedpenutupKatub,
                    onChanged: (String? value){
                      setState(() {
                        _selectedpenutupKatub = value;
                      });
                    },
                  ),
                )
              ],
            ),
          ),

          const SizedBox(height: 28),

          // Pipa penghubung tube refrigerant antara unit indoor dan outdoor tidak penyok
          CheckboxFormCustom(
            textLabel: 'Pipa penghubung tube refrigerant antara unit indoor dan outdoor tidak penyok',
            listCheckbox: Row(
              children: [
                Expanded(
                  child: CustomCheckbox(
                    value: 'Baik',
                    groupValue: _selectedPipaPenghubung,
                    onChanged: (String? value){
                      setState(() {
                        _selectedPipaPenghubung = value;
                      });
                    },
                  ),
                ),

                Expanded(
                  child: CustomCheckbox(
                    value: 'Tidak Baik',
                    groupValue: _selectedPipaPenghubung,
                    onChanged: (String? value){
                      setState(() {
                        _selectedPipaPenghubung = value;
                      });
                    },
                  ),
                )
              ],
            ),
          ),

          const SizedBox(height: 8),
        ],
      ),
    );
  }

  Widget bottomButtons(BuildContext context){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        // Batalkan
        FilledButtonCustom(
          width: MediaQuery.sizeOf(context).width * 0.38,
          height: 56,
          buttonColor: const Color(0xffEB5757),
          label: Text(
            'Kembali',
            style: whiteTextStyle.copyWith(
                fontSize: 16,
                fontWeight: bold
            ),
          ),

          onTap: (){
            Navigator.pop(context);
          },
        ),

        // Lanjutkan
        FilledButtonCustom(
          width: MediaQuery.sizeOf(context).width * 0.45,
          height: 56,
          buttonColor: const Color(0xff04AFD2),
          label: Text(
            'Lanjutkan',
            style: whiteTextStyle.copyWith(
                fontSize: 16,
                fontWeight: bold
            ),
          ),

          onTap: (){
            Navigator.pushNamed(context, '/form-cek-fisik-outdoor-2');
          },
        ),
      ],
    );
  }
}

