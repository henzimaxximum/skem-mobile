import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:skem/layouts/custom_widget/form/inspeksi_input_custom.dart';
import 'package:skem/models/form_item_model.dart';

import '../../../../layouts/custom_widget/buttons/filled_button_custom.dart';
import '../../../../layouts/custom_widget/form/checkbox_form_custom.dart';
import '../../../../layouts/custom_widget/form/dropdown_custom.dart';
import '../../../../layouts/custom_widget/form/dropdown_item_custom.dart';
import '../../../../layouts/custom_widget/form/inspeksi_form_card.dart';
import '../../../../layouts/custom_widget/page_layout/blue_linear_background.dart';
import '../../../../shared/shared_theme/fonts.dart';
import 'cek_fisik_form_2_questions.dart';

class FormCekFisikOutdoor2Screen extends StatefulWidget {
  const FormCekFisikOutdoor2Screen({super.key});

  @override
  State<FormCekFisikOutdoor2Screen> createState() => _FormCekFisikOutdoor2ScreenState();
}

class _FormCekFisikOutdoor2ScreenState extends State<FormCekFisikOutdoor2Screen> {
  FormItemModel? selectedHasilPenilaian;

  String? _selectedKondisiKemasanAC;
  String? _selectedKondisiFisikAC;
  String? _selectedKondisiFinSirip;
  String? _selectedKondisiKipasUnitOutdoor;
  String? _selectedpenutupKatub;
  String? _selectedPipaWay;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.white,
        elevation: 0.5,
        toolbarHeight: 60,

        // Icon Arrow Back
        leading: IconButton(
          onPressed: (){
            Navigator.pop(context);
          },

          icon: const Icon(
            Icons.arrow_back,
            size: 26,
            color: Colors.black,
          ),
        ),

        // Page Title
        title: Text(
          'Cek Fisik: Unit Outdoor',
          textAlign: TextAlign.center,
          maxLines: 2,
          style: blackTextStyle.copyWith(
            fontSize: 18,
            fontWeight: bold,
          ),
        ),
      ),

      body: BlueLinearBackground(
        middleStop: 0.2,
        child: Stack(
          children: [
            // Icon Kuning Kiri Atas
            Positioned(
              top: -25,
              left: -55,

              child: SvgPicture.asset(
                'assets/icon/svg_circle_kanan_bawah.svg',
                width: 220,
                height: 220,
              ),
            ),

            // Icon Kuning Kanan Bawah
            Positioned(
              bottom: -25,
              right: -55,

              child: SvgPicture.asset(
                'assets/icon/svg_circle_kanan_bawah.svg',
                width: 220,
                height: 220,
              ),
            ),

            // Main Page Content
            ListView(
              padding: const EdgeInsets.all(20),
              children: [
                formCekFisikIndoor(),

                const SizedBox(height: 25),

                // Batalkan & Lanjutkan Button
                bottomButtons(context),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget formCekFisikIndoor(){
    return InspeksiFormCard(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // Kondisi kemasan produk AC tidak rusak
          CheckboxFormCustom(
            textLabel: 'Kondisi kemasan produk AC tidak rusak',
            listCheckbox: Row(
              children: [
                Expanded(
                  child: CustomCheckbox(
                    value: 'Baik',
                    groupValue: _selectedKondisiKemasanAC,
                    onChanged: (String? value){
                      setState(() {
                        _selectedKondisiKemasanAC = value;
                      });
                    },
                  ),
                ),

                Expanded(
                  child: CustomCheckbox(
                    value: 'Tidak Baik',
                    groupValue: _selectedKondisiKemasanAC,
                    onChanged: (String? value){
                      setState(() {
                        _selectedKondisiKemasanAC = value;
                      });
                    },
                  ),
                )
              ],
            ),
          ),

          const SizedBox(height: 28),

          // Kondisi fisik produk AC secara visual seperti bodi unit indoor
          CheckboxFormCustom(
            textLabel: 'Kondisi fisik produk AC secara visual seperti bodi unit indoor',
            listCheckbox: Row(
              children: [
                Expanded(
                  child: CustomCheckbox(
                    value: 'Baik',
                    groupValue: _selectedKondisiFisikAC,
                    onChanged: (String? value){
                      setState(() {
                        _selectedKondisiFisikAC = value;
                      });
                    },
                  ),
                ),

                Expanded(
                  child: CustomCheckbox(
                    value: 'Tidak Baik',
                    groupValue: _selectedKondisiFisikAC,
                    onChanged: (String? value){
                      setState(() {
                        _selectedKondisiFisikAC = value;
                      });
                    },
                  ),
                )
              ],
            ),
          ),

          const SizedBox(height: 28),

          // Kondisi fin (sirip) alat penukar kalor tidak rusak, deformasi, atau berubah warna pada unit outdoor
          CheckboxFormCustom(
            textLabel: 'Kondisi fin (sirip) alat penukar kalor tidak rusak, deformasi, atau berubah warna pada unit outdoor',
            listCheckbox: Row(
              children: [
                Expanded(
                  child: CustomCheckbox(
                    value: 'Baik',
                    groupValue: _selectedKondisiFinSirip,
                    onChanged: (String? value){
                      setState(() {
                        _selectedKondisiFinSirip = value;
                      });
                    },
                  ),
                ),

                Expanded(
                  child: CustomCheckbox(
                    value: 'Tidak Baik',
                    groupValue: _selectedKondisiFinSirip,
                    onChanged: (String? value){
                      setState(() {
                        _selectedKondisiFinSirip = value;
                      });
                    },
                  ),
                )
              ],
            ),
          ),

          const SizedBox(height: 28),

          // Kondisi kipas pada unit indoor tidak retak, rusak, atau menyentuh bagian lainnya
          CheckboxFormCustom(
            textLabel: 'Kondisi kipas pada unit indoor tidak retak, rusak, atau menyentuh bagian lainnya',
            listCheckbox: Row(
              children: [
                Expanded(
                  child: CustomCheckbox(
                    value: 'Baik',
                    groupValue: _selectedKondisiKipasUnitOutdoor,
                    onChanged: (String? value){
                      setState(() {
                        _selectedKondisiKipasUnitOutdoor = value;
                      });
                    },
                  ),
                ),

                Expanded(
                  child: CustomCheckbox(
                    value: 'Tidak Baik',
                    groupValue: _selectedKondisiKipasUnitOutdoor,
                    onChanged: (String? value){
                      setState(() {
                        _selectedKondisiKipasUnitOutdoor = value;
                      });
                    },
                  ),
                )
              ],
            ),
          ),

          const SizedBox(height: 28),

          // Terdapat penutup katub atau tidak ada kerusakan pada ulir sambungan pipa antara unit indoor
          CheckboxFormCustom(
            textLabel: 'Terdapat penutup katub atau tidak ada kerusakan pada ulir sambungan pipa antara unit indoor',
            listCheckbox: Row(
              children: [
                Expanded(
                  child: CustomCheckbox(
                    value: 'Baik',
                    groupValue: _selectedpenutupKatub,
                    onChanged: (String? value){
                      setState(() {
                        _selectedpenutupKatub = value;
                      });
                    },
                  ),
                ),

                Expanded(
                  child: CustomCheckbox(
                    value: 'Tidak Baik',
                    groupValue: _selectedpenutupKatub,
                    onChanged: (String? value){
                      setState(() {
                        _selectedpenutupKatub = value;
                      });
                    },
                  ),
                )
              ],
            ),
          ),

          const SizedBox(height: 28),

          // Pipa 2-way dan 3-way valve dalam keadaan tertutup (tidak ada kebocoran gas)
          CheckboxFormCustom(
            textLabel: 'Pipa 2-way dan 3-way valve dalam keadaan tertutup (tidak ada kebocoran gas)',
            listCheckbox: Row(
              children: [
                Expanded(
                  child: CustomCheckbox(
                    value: 'Baik',
                    groupValue: _selectedPipaWay,
                    onChanged: (String? value){
                      setState(() {
                        _selectedPipaWay = value;
                      });
                    },
                  ),
                ),

                Expanded(
                  child: CustomCheckbox(
                    value: 'Tidak Baik',
                    groupValue: _selectedPipaWay,
                    onChanged: (String? value){
                      setState(() {
                        _selectedPipaWay = value;
                      });
                    },
                  ),
                )
              ],
            ),
          ),

          const SizedBox(height: 28),

          // Dropdown Hasil Penilaian
          hasilPenilaianDropdown(),

          const SizedBox(height: 28),

          // Hasil Penilaian
          InspeksiInputCustom(
            formLabel: 'Hasil Penilaian',
            hintText: 'Masukkan keterangan tambahan (opsional)',
          ),

          const SizedBox(height: 8),
        ],
      ),
    );
  }

  Widget hasilPenilaianDropdown(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // Text Hasil Penlilaian
        Text(
          'Hasil Penilaian',
          style: blackTextStyle.copyWith(
            fontSize: 13,
            fontWeight: semiBold,
          ),
        ),

        const SizedBox(height: 16),

        DropdownCustom(
          dropdownText: selectedHasilPenilaian?.text,
          child: Column(
            children: listHasilPenilianOutdoor.map((itemLokasi){
              return GestureDetector(
                onTap: (){
                  selectedHasilPenilaian = itemLokasi;
                  setState(() {});
                },

                child: DropdownItemCustom(
                  formItem: itemLokasi,
                  isItemSelected: selectedHasilPenilaian?.id == itemLokasi.id,
                ),
              );
            }).toList(),
          ),
        )
      ],
    );
  }

  Widget bottomButtons(BuildContext context){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        // Batalkan
        FilledButtonCustom(
          width: MediaQuery.sizeOf(context).width * 0.38,
          height: 56,
          buttonColor: const Color(0xffEB5757),
          label: Text(
            'Kembali',
            style: whiteTextStyle.copyWith(
                fontSize: 16,
                fontWeight: bold
            ),
          ),

          onTap: (){
            Navigator.pop(context);
          },
        ),

        // Lanjutkan
        FilledButtonCustom(
          width: MediaQuery.sizeOf(context).width * 0.45,
          height: 56,
          buttonColor: const Color(0xff04AFD2),
          label: Text(
            'Lanjutkan',
            style: whiteTextStyle.copyWith(
                fontSize: 16,
                fontWeight: bold
            ),
          ),

          onTap: (){
            Navigator.pushNamed(context, '/form-hasil-final-pengujian-2');
          },
        ),
      ],
    );
  }
}
