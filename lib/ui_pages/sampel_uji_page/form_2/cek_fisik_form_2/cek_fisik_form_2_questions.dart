import '../../../../models/form_item_model.dart';

// Hasil Penilaian
List<FormItemModel> listHasilPenilianOutdoor = [
  FormItemModel(id: 1, text: 'Baik, dapat dilanjutkan ke pengecekan kondisi pengujian'),
  FormItemModel(id: 2, text: 'Tidak diterima, unit dikemas kembali untuk dikembalikan'),
];