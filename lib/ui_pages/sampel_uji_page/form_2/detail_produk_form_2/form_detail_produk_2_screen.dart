import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:skem/layouts/custom_widget/form/inspeksi_form_card.dart';

import '../../../../layouts/custom_widget/buttons/filled_button_custom.dart';
import '../../../../layouts/custom_widget/form/inspeksi_input_custom.dart';
import '../../../../layouts/custom_widget/page_layout/blue_linear_background.dart';
import '../../../../shared/shared_theme/fonts.dart';

class FormDetailProduk2Screen extends StatelessWidget {
  const FormDetailProduk2Screen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.white,
        elevation: 0.5,
        toolbarHeight: 60,

        // Icon Arrow Back
        leading: IconButton(
          onPressed: (){
            Navigator.pop(context);
          },

          icon: const Icon(
            Icons.arrow_back,
            size: 26,
            color: Colors.black,
          ),
        ),

        // Page Title
        title: Text(
          'Detail Produk',
          textAlign: TextAlign.center,
          maxLines: 2,
          style: blackTextStyle.copyWith(
            fontSize: 18,
            fontWeight: bold,
          ),
        ),
      ),

      body: BlueLinearBackground(
        middleStop: 0.2,
        child: Stack(
          children: [
            // Icon Kuning Kiri Atas
            Positioned(
              top: -25,
              left: -55,

              child: SvgPicture.asset(
                'assets/icon/svg_circle_kanan_bawah.svg',
                width: 220,
                height: 220,
              ),
            ),

            // Icon Kuning Kanan Bawah
            Positioned(
              bottom: -25,
              right: -55,

              child: SvgPicture.asset(
                'assets/icon/svg_circle_kanan_bawah.svg',
                width: 220,
                height: 220,
              ),
            ),

            // Main Page Content
            ListView(
              padding: const EdgeInsets.all(20),
              children: [
                const SizedBox(height: 10),

                // Section Form Detail Pemeriksaan Visual
                formDetailProduk(),

                const SizedBox(height: 30),

                // Batalkan & Lanjutkan Button
                bottomButtons(context),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget formDetailProduk(){
    return InspeksiFormCard(
      child: const Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // Merek
          InspeksiInputCustom(
            formLabel: 'Merek',
            hintText: 'Otomatis terisi dan tidak dapat diubah',
          ),

          SizedBox(height: 16),

          // Metrik 1: Kapasitas
          InspeksiInputCustom(
            formLabel: 'Metrik 1: Kapasitas',
            hintText: 'Otomatis terisi dan tidak dapat diubah',
          ),

          SizedBox(height: 16),

          // Metrik 2: Teknologi
          InspeksiInputCustom(
            formLabel: 'Metrik 2: Teknologi',
            hintText: 'Otomatis terisi dan tidak dapat diubah',
          ),

          SizedBox(height: 16),

          // Metrik 3: (Empty)
          InspeksiInputCustom(
            formLabel: 'Metrik 3: (Empty)',
            hintText: 'Otomatis terisi dan tidak dapat diubah',
          ),

          SizedBox(height: 16),

          // Manufaktur / Improtir
          InspeksiInputCustom(
            formLabel: 'Manufaktur / Improtir',
            hintText: 'Otomatis terisi dan tidak dapat diubah',
          ),

          SizedBox(height: 16),

          // Kode atau Tanggal Produksi
          InspeksiInputCustom(
            formLabel: 'Kode atau Tanggal Produksi',
            hintText: 'Otomatis terisi dan tidak dapat diubah',
          ),

          SizedBox(height: 16),

          // Negara Asal
          InspeksiInputCustom(
            formLabel: 'Negara Asal',
            hintText: 'Otomatis terisi dan tidak dapat diubah',
          ),
        ],
      ),
    );
  }

  Widget bottomButtons(BuildContext context){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        // Batalkan
        FilledButtonCustom(
          width: MediaQuery.sizeOf(context).width * 0.38,
          height: 56,
          buttonColor: const Color(0xffEB5757),
          label: Text(
            'Kembali',
            style: whiteTextStyle.copyWith(
                fontSize: 16,
                fontWeight: bold
            ),
          ),

          onTap: (){
            Navigator.pop(context);
          },
        ),

        // Lanjutkan
        FilledButtonCustom(
          width: MediaQuery.sizeOf(context).width * 0.45,
          height: 56,
          buttonColor: const Color(0xff04AFD2),
          label: Text(
            'Lanjutkan',
            style: whiteTextStyle.copyWith(
                fontSize: 16,
                fontWeight: bold
            ),
          ),

          onTap: (){
            Navigator.pushNamed(context, '/form-pemeriksaan-visual-2');
          },
        ),
      ],
    );
  }
}
