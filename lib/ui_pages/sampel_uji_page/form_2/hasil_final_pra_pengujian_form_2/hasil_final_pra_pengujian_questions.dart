import '../../../../models/form_item_model.dart';

// Hasil Penilaian
List<FormItemModel> listHasilPenilianPraPengujian = [
  FormItemModel(id: 1, text: 'Baik, pengujian dapat dimulai'),
  FormItemModel(id: 2, text: 'Tidak diterima, unit dikemas kembali untuk dikembalikan'),
];