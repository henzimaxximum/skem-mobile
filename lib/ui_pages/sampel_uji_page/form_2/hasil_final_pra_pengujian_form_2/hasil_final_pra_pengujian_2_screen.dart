import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:skem/models/form_item_model.dart';

import '../../../../layouts/custom_widget/buttons/filled_button_custom.dart';
import '../../../../layouts/custom_widget/form/dropdown_custom.dart';
import '../../../../layouts/custom_widget/form/dropdown_item_custom.dart';
import '../../../../layouts/custom_widget/form/inspeksi_form_card.dart';
import '../../../../layouts/custom_widget/form/inspeksi_input_custom.dart';
import '../../../../layouts/custom_widget/page_layout/blue_linear_background.dart';
import '../../../../shared/shared_theme/fonts.dart';
import 'hasil_final_pra_pengujian_questions.dart';

class HasilFinalPraPengujian2Screen extends StatefulWidget {
  const HasilFinalPraPengujian2Screen({super.key});

  @override
  State<HasilFinalPraPengujian2Screen> createState() => _HasilFinalPraPengujian2ScreenState();
}

class _HasilFinalPraPengujian2ScreenState extends State<HasilFinalPraPengujian2Screen> {
  FormItemModel? selectedHasilPenilaianPraPengujian;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.white,
        elevation: 0.5,
        toolbarHeight: 60,

        // Icon Arrow Back
        leading: IconButton(
          onPressed: (){
            Navigator.pop(context);
          },

          icon: const Icon(
            Icons.arrow_back,
            size: 26,
            color: Colors.black,
          ),
        ),

        // Page Title
        title: Text(
          'Cek Fisik: Unit Outdoor',
          textAlign: TextAlign.center,
          maxLines: 2,
          style: blackTextStyle.copyWith(
            fontSize: 18,
            fontWeight: bold,
          ),
        ),
      ),

      body: BlueLinearBackground(
        middleStop: 0.2,
        child: Stack(
          children: [
            // Icon Kuning Kiri Atas
            Positioned(
              top: -25,
              left: -55,

              child: SvgPicture.asset(
                'assets/icon/svg_circle_kanan_bawah.svg',
                width: 220,
                height: 220,
              ),
            ),

            // Icon Kuning Kanan Bawah
            Positioned(
              bottom: -25,
              right: -55,

              child: SvgPicture.asset(
                'assets/icon/svg_circle_kanan_bawah.svg',
                width: 220,
                height: 220,
              ),
            ),

            // Main Page Content
            ListView(
              padding: const EdgeInsets.all(20),
              children: [
                formHasilFinalPraPengujian(),

                const SizedBox(height: 25),

                // Batalkan & Lanjutkan Button
                bottomButtons(context),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget formHasilFinalPraPengujian(){
    return InspeksiFormCard(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // Dropdown Hasil Penilaian
          hasilPenilaianDropdown(),

          const SizedBox(height: 28),

          // Keterangan Tambahan
          InspeksiInputCustom(
            formLabel: 'Keterangan Tambahan',
            hintText: 'Masukkan keterangan tambahan (opsional)',
          ),

          const SizedBox(height: 8),
        ],
      ),
    );
  }

  Widget hasilPenilaianDropdown(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // Text Hasil Penlilaian
        Text(
          'Hasil Penilaian',
          style: blackTextStyle.copyWith(
            fontSize: 13,
            fontWeight: semiBold,
          ),
        ),

        const SizedBox(height: 16),

        DropdownCustom(
          dropdownText: selectedHasilPenilaianPraPengujian?.text,
          child: Column(
            children: listHasilPenilianPraPengujian.map((itemLokasi){
              return GestureDetector(
                onTap: (){
                  selectedHasilPenilaianPraPengujian = itemLokasi;
                  setState(() {});
                },

                child: DropdownItemCustom(
                  formItem: itemLokasi,
                  isItemSelected: selectedHasilPenilaianPraPengujian?.id == itemLokasi.id,
                ),
              );
            }).toList(),
          ),
        )
      ],
    );
  }

  Widget bottomButtons(BuildContext context){
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        // Simpan dan Selesai
        FilledButtonCustom(
          width: MediaQuery.sizeOf(context).width * 0.43,
          height: 60,
          buttonColor: const Color(0xff04AFD2),
          label: Text(
            'Simpan dan\nSelesai',
            textAlign: TextAlign.center,
            style: whiteTextStyle.copyWith(
                fontSize: 16,
                fontWeight: bold
            ),
          ),

          onTap: (){},
        ),
      ],
    );
  }
}
