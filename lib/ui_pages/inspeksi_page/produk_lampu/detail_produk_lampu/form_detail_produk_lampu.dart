import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../../layouts/custom_widget/buttons/filled_button_custom.dart';
import '../../../../layouts/custom_widget/form/inspeksi_input_custom.dart';
import '../../../../layouts/custom_widget/page_layout/blue_linear_background.dart';
import '../../../../layouts/custom_widget/form/inspeksi_form_card.dart';
import '../../../../shared/shared_theme/fonts.dart';

class FormDetailProdukLampuScreen extends StatelessWidget {
  const FormDetailProdukLampuScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.white,
        elevation: 0.6,
        // Icon Arrow Back
        leading: IconButton(
          onPressed: (){
            Navigator.pop(context);
          },

          icon: const Icon(
            Icons.arrow_back,
            size: 26,
            color: Colors.black,
          ),
        ),

        // Page Title
        title: Text(
          'Detail Produk',
          style: blackTextStyle.copyWith(
            fontSize: 20,
            fontWeight: bold,
          ),
        ),
      ),

      body: BlueLinearBackground(
        middleStop: 0.2,
        child: Stack(
          children: [
            // Icon Kuning Kiri Atas
            Positioned(
              top: -25,
              left: -55,

              child: SvgPicture.asset(
                'assets/icon/svg_circle_kiri_atas.svg',
                width: 220,
                height: 220,
              ),
            ),

            // Icon Kuning Kanan Bawah
            Positioned(
              bottom: -25,
              right: -55,

              child: SvgPicture.asset(
                'assets/icon/svg_circle_kanan_bawah.svg',
                width: 220,
                height: 220,
              ),
            ),

            // Main Page Content
            ListView(
              padding: const EdgeInsets.all(20),
              children: [
                // Form Content
                InspeksiFormCard(
                  child: Column(
                    children: [
                      // Form Input Merk
                      const InspeksiInputCustom(
                        formLabel: 'Merek',
                        hintText: 'Masukkan merek produk',
                      ),

                      const SizedBox(height: 25),

                      // Form Input Daya Pengenal
                      const InspeksiInputCustom(
                        formLabel: 'Daya Pengenal (W)',
                        hintText: 'Masukkan daya pengenal',
                      ),

                      const SizedBox(height: 25),

                      // Form Input Arus Cahaya Pengenal (Lumen)
                      const InspeksiInputCustom(
                        formLabel: 'Arus Cahaya Pengenal (Lumen)',
                        hintText: 'Masukkan nilai lumen',
                      ),

                      const SizedBox(height: 25),

                      // Form Input Teknologi
                      const InspeksiInputCustom(
                        formLabel: 'Teknologi',
                        hintText: 'Masukkan teknologi produk',
                      ),

                      const SizedBox(height: 25),

                      // Form Input Manufaktur/Importir
                      const InspeksiInputCustom(
                        formLabel: 'Manufaktur/Importir',
                        hintText: 'Masukkan data importir',
                      ),

                      const SizedBox(height: 25),

                      // Form Input Kode atau Tanggal Produksi
                      const InspeksiInputCustom(
                        formLabel: 'Kode atau Tanggal Produksi',
                        hintText: 'Masukkan Kode atau Tanggal Produksi',
                        inputType: TextInputType.number,
                      ),

                      const SizedBox(height: 25),

                      // Form Input Negara Asal
                      const InspeksiInputCustom(
                        formLabel: 'Negara Asal',
                        hintText: 'Masukkan Negara Asal',
                      ),

                      const SizedBox(height: 25),

                      // Form Input Harga
                      const InspeksiInputCustom(
                        formLabel: 'Harga',
                        hintText: 'Masukkan Harga',
                        inputType: TextInputType.number,
                      ),

                      const SizedBox(height: 25),

                      // Form Model
                      const InspeksiInputCustom(
                        formLabel: 'Model',
                        hintText: 'Masukkan Model',
                      ),
                    ],
                  ),
                ),

                const SizedBox(height: 20),

                // Kembali & Lanjutkan Button
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    // Batalkan
                    FilledButtonCustom(
                      width: MediaQuery.sizeOf(context).width * 0.38,
                      height: 56,
                      buttonColor: const Color(0xffEB5757),
                      label: Text(
                        'Kembali',
                        style: whiteTextStyle.copyWith(
                            fontSize: 16,
                            fontWeight: bold
                        ),
                      ),

                      onTap: (){
                        Navigator.pop(context);
                      },
                    ),

                    // Lanjutkan
                    FilledButtonCustom(
                      width: MediaQuery.sizeOf(context).width * 0.45,
                      height: 56,
                      buttonColor: const Color(0xff04AFD2),
                      label: Text(
                        'Lanjutkan',
                        style: whiteTextStyle.copyWith(
                            fontSize: 16,
                            fontWeight: bold
                        ),
                      ),

                      onTap: (){
                        Navigator.pushNamed(context, '/form-compliance-lthe-lampu');
                      },
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
