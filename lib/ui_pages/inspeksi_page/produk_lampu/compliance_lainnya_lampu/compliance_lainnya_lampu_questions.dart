import 'package:skem/models/form_item_model.dart';

// Apakah label SNI tercantum dan dapat terbaca jelas
List<FormItemModel> listSNITercantumLampu = [
  FormItemModel(id: 1, text: 'Ya'),
  FormItemModel(id: 2, text: 'Tidak Yakin'),
  FormItemModel(id: 3, text: 'Tidak'),
];

// Apakah Ekolabel tercantum dan dapat terbaca jelas
List<FormItemModel> listEkolabelTercantumLampu = [
  FormItemModel(id: 1, text: 'Ya'),
  FormItemModel(id: 2, text: 'Tidak Yakin'),
  FormItemModel(id: 3, text: 'Tidak'),
];

// Apakah kartu garansi tercantum dan dapat terbaca jelas
List<FormItemModel> listKartuGaransiTercantumLampu = [
  FormItemModel(id: 1, text: 'Ya'),
  FormItemModel(id: 2, text: 'Tidak Yakin'),
  FormItemModel(id: 3, text: 'Tidak'),
];

// Apakah produk dalam bahasa indonesia
List<FormItemModel> listPordukBahasaIndonesiaLampu = [
  FormItemModel(id: 1, text: 'Ya'),
  FormItemModel(id: 2, text: 'Tidak Yakin'),
  FormItemModel(id: 3, text: 'Tidak'),
];