import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:skem/ui_pages/inspeksi_page/produk_lampu/compliance_lthe_lampu/compliance_lthe_lampu_questions.dart';

import '../../../../layouts/custom_widget/buttons/filled_button_custom.dart';
import '../../../../layouts/custom_widget/form/dropdown_custom.dart';
import '../../../../layouts/custom_widget/form/dropdown_item_custom.dart';
import '../../../../layouts/custom_widget/form/inspeksi_input_custom.dart';
import '../../../../layouts/custom_widget/page_layout/blue_linear_background.dart';
import '../../../../layouts/custom_widget/form/inspeksi_form_card.dart';
import '../../../../models/form_item_model.dart';
import '../../../../shared/shared_theme/fonts.dart';

class FormComplianceLanjutLampu extends StatefulWidget {
  const FormComplianceLanjutLampu({super.key});

  @override
  State<FormComplianceLanjutLampu> createState() => _FormComplianceLanjutLampuState();
}

class _FormComplianceLanjutLampuState extends State<FormComplianceLanjutLampu> {
  FormItemModel? selectedTinjauanItem;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.white,
        elevation: 0.6,
        // Icon Arrow Back
        leading: IconButton(
          onPressed: (){
            Navigator.pop(context);
          },

          icon: const Icon(
            Icons.arrow_back,
            size: 26,
            color: Colors.black,
          ),
        ),

        // Page Title
        title: Text(
          'Compliance LTHE',
          style: blackTextStyle.copyWith(
            fontSize: 20,
            fontWeight: bold,
          ),
        ),
      ),

      body: BlueLinearBackground(
        middleStop: 0.2,
        child: Stack(
          children: [
            // Icon Kuning Kiri Atas
            Positioned(
              top: -25,
              left: -55,

              child: SvgPicture.asset(
                'assets/icon/svg_circle_kiri_atas.svg',
                width: 220,
                height: 220,
              ),
            ),

            // Icon Kuning Kanan Bawah
            Positioned(
              bottom: -25,
              right: -55,

              child: SvgPicture.asset(
                'assets/icon/svg_circle_kanan_bawah.svg',
                width: 220,
                height: 220,
              ),
            ),

            // Main Page Content
            ListView(
              padding: const EdgeInsets.all(20),
              children: [
                // Form Content
                InspeksiFormCard(
                  child: Column(
                    children: [
                      // Form Input Ketidaksesuaian LTHE
                      InspeksiInputCustom(
                        formLabel: 'Ketidaksesuaian LTHE',
                        hintText: 'Jelaskan ketidaksesuaian yang ditemukan',
                      ),

                      const SizedBox(height: 25),

                      formTinjauanInspeksi(),

                      const SizedBox(height: 25),

                      // Form Input Mencurigakan
                      InspeksiInputCustom(
                        formLabel: 'Tuliskan penjelasan apabila label atau produk mencurigakan',
                        hintText: 'Jelaskan ketidaksesuaian',
                      ),
                    ],
                  ),
                ),

                const SizedBox(height: 40),

                // Kembali & Lanjutkan Button
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    // Batalkan Button
                    FilledButtonCustom(
                      width: MediaQuery.sizeOf(context).width * 0.38,
                      height: 56,
                      buttonColor: const Color(0xffEB5757),
                      label: Text(
                        'Kembali',
                        style: whiteTextStyle.copyWith(
                            fontSize: 16,
                            fontWeight: bold
                        ),
                      ),

                      onTap: (){
                        Navigator.pop(context);
                      },
                    ),

                    // Lanjutkan Button
                    FilledButtonCustom(
                      width: MediaQuery.sizeOf(context).width * 0.45,
                      height: 56,
                      buttonColor: const Color(0xff04AFD2),
                      label: Text(
                        'Lanjutkan',
                        style: whiteTextStyle.copyWith(
                            fontSize: 16,
                            fontWeight: bold
                        ),
                      ),

                      onTap: (){
                        Navigator.pushNamed(context, '/form-pemeriksaan-visual-lampu');
                      },
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget formTinjauanInspeksi(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // Text Tipe Lokasi Pengawasan
        Text(
          'Tinjauan Inspeksi LTHE',
          style: blackTextStyle.copyWith(
            fontSize: 15,
            fontWeight: semiBold,
          ),
        ),

        const SizedBox(height: 16),

        DropdownCustom(
          dropdownText: selectedTinjauanItem?.text,
          child: Column(
            children: listTinjauanInspeksiLampu.map((itemTinjauan){
              return GestureDetector(
                onTap: (){
                  selectedTinjauanItem = itemTinjauan;
                  setState(() {});
                },

                child: DropdownItemCustom(
                  formItem: itemTinjauan,
                  isItemSelected: selectedTinjauanItem?.id == itemTinjauan.id,
                ),
              );
            }).toList(),
          ),
        )
      ],
    );
  }
}
