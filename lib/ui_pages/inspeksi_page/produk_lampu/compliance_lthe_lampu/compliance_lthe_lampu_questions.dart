import 'package:skem/models/form_item_model.dart';

// Apakah LTHE tercantum pada produk
List<FormItemModel> listTercantumProdukLampu = [
  FormItemModel(id: 1, text: 'Ya'),
  FormItemModel(id: 2, text: 'Tidak'),
];

// Apakah LTHE tercantum pada kotak kemasan
List<FormItemModel> listTercantumKemasanLampu = [
  FormItemModel(id: 1, text: 'Ya'),
  FormItemModel(id: 2, text: 'Tidak'),
];

// Visibilitas LTHE
List<FormItemModel> listVisibilitasLTHELampu = [
  FormItemModel(id: 1, text: 'Label jelas dan mudah terlihat'),
  FormItemModel(id: 2, text: 'Label kabur atau rusak karena tindakan produsen atau importir'),
  FormItemModel(id: 3, text: 'Label sebagian atau seluruhnya tertutup dengan label lain atau informasi pemasaran'),
  FormItemModel(id: 4, text: 'Tidak berlaku - label tidak dibutuhkan'),
];

// Kesesuain Visual LTHE
List<FormItemModel> listKesesuaianVisualLampu = [
  FormItemModel(id: 1, text: 'Label terlihat benar dan sesuai dengan persyaratan'),
  FormItemModel(id: 2, text: 'Desain label salah (warna, ukuran, dll)'),
  FormItemModel(id: 3, text: 'Label tampak palsu'),
  FormItemModel(id: 4, text: 'Label tidak sesuai dengan model fisik produk'),
  FormItemModel(id: 5, text: 'Tidak berlaku - label tidak dibutuhkan'),
];

// Tujuan Inspeksi LTHE
List<FormItemModel> listTinjauanInspeksiLampu = [
  FormItemModel(id: 1, text: 'Label dan produk tampak sesuai'),
  FormItemModel(id: 2, text: 'Label atau produk tampak mencurigakan dan memerlukan penyelidikan lebih lanjut'),
  FormItemModel(id: 3, text: 'Label atau produk tampak mencurigakan dan direkomendasikan untuk pengujian verifikasi'),
];