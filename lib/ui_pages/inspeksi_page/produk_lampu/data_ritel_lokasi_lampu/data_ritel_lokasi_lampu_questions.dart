import '../../../../models/form_item_model.dart';

List<FormItemModel> listTipeLokasiPengawasanLampu = [
  FormItemModel(id: 1, text: 'Warung'),
  FormItemModel(id: 2, text: 'Pasar Tradisional'),
  FormItemModel(id: 3, text: 'Toko'),
  FormItemModel(id: 4, text: 'Dealer/Distributor'),
  FormItemModel(id: 5, text: 'Chain'),
  FormItemModel(id: 6, text: 'Department Store'),
  FormItemModel(id: 7, text: 'Hypermarket'),
  FormItemModel(id: 8, text: 'General Store'),
  FormItemModel(id: 9, text: 'Supermarket'),
  FormItemModel(id: 10, text: 'Gudang Produsen / Importir'),
];