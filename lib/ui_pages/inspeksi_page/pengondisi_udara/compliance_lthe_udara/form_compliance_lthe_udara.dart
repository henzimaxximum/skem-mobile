import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../../layouts/custom_widget/buttons/filled_button_custom.dart';
import '../../../../layouts/custom_widget/form/dropdown_custom.dart';
import '../../../../layouts/custom_widget/form/dropdown_item_custom.dart';
import '../../../../layouts/custom_widget/page_layout/blue_linear_background.dart';
import '../../../../layouts/custom_widget/form/inspeksi_form_card.dart';
import '../../../../models/form_item_model.dart';
import '../../../../shared/shared_theme/fonts.dart';
import 'compliance_lthe_udara_questions.dart';

class FormComlplianceLTHEUdaraScreen extends StatefulWidget {
  const FormComlplianceLTHEUdaraScreen({super.key});

  @override
  State<FormComlplianceLTHEUdaraScreen> createState() => _FormComlplianceLTHEUdaraScreenState();
}

class _FormComlplianceLTHEUdaraScreenState extends State<FormComlplianceLTHEUdaraScreen> {
  FormItemModel? selectedTercantumProdukItem;
  FormItemModel? selectedTercantumKemasanItem;
  FormItemModel? selectedVisibilitasItem;
  FormItemModel? selectedVisualItem;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.white,
        elevation: 0.6,
        // Icon Arrow Back
        leading: IconButton(
          onPressed: (){
            Navigator.pop(context);
          },

          icon: const Icon(
            Icons.arrow_back,
            size: 26,
            color: Colors.black,
          ),
        ),

        // Page Title
        title: Text(
          'Compliance LTHE',
          style: blackTextStyle.copyWith(
            fontSize: 20,
            fontWeight: bold,
          ),
        ),
      ),

      body: BlueLinearBackground(
        middleStop: 0.2,
        child: Stack(
          children: [
            // Icon Kuning Kanan Bawah
            Positioned(
              bottom: -25,
              right: -55,

              child: SvgPicture.asset(
                'assets/icon/svg_circle_kanan_bawah.svg',
                width: 220,
                height: 220,
              ),
            ),

            // Main Page Content
            ListView(
              padding: const EdgeInsets.all(20),
              children: [
                // Form Content
                InspeksiFormCard(
                  child: Column(
                    children: [
                      // Dropdown Tercantum Pada Produk
                      formTercantumProduk(),

                      const SizedBox(height: 25),

                      // Dropdown Tercantum Pada Kotak Kemasan
                      formTercantumKemasan(),

                      const SizedBox(height: 25),

                      // Dropdown Visibilitas LTHE
                      formVisibilitasLTHE(),

                      const SizedBox(height: 25),

                      // Dropdown Visibilitas LTHE
                      formVisualLTHE(),
                    ],
                  ),
                ),

                const SizedBox(height: 40),

                // Kembali & Lanjutkan Button
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    // Batalkan Button
                    FilledButtonCustom(
                      width: MediaQuery.sizeOf(context).width * 0.38,
                      height: 56,
                      buttonColor: const Color(0xffEB5757),
                      label: Text(
                        'Kembali',
                        style: whiteTextStyle.copyWith(
                            fontSize: 16,
                            fontWeight: bold
                        ),
                      ),

                      onTap: (){
                        Navigator.pop(context);
                      },
                    ),

                    // Lanjutkan Button
                    FilledButtonCustom(
                      width: MediaQuery.sizeOf(context).width * 0.45,
                      height: 56,
                      buttonColor: const Color(0xff04AFD2),
                      label: Text(
                        'Lanjutkan',
                        style: whiteTextStyle.copyWith(
                            fontSize: 16,
                            fontWeight: bold
                        ),
                      ),

                      onTap: (){
                        Navigator.pushNamed(context, '/form-compliance-lanjut-udara');
                      },
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget formTercantumProduk(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // Text Tipe Lokasi Pengawasan
        Text(
          'Apakah LTHE tercantum pada produk?',
          style: blackTextStyle.copyWith(
            fontSize: 15,
            fontWeight: semiBold,
          ),
        ),

        const SizedBox(height: 16),

        DropdownCustom(
          dropdownText: selectedTercantumProdukItem?.text,
          child: Column(
            children: listTercantumProduk.map((itemTercantumProduk){
              return GestureDetector(
                onTap: (){
                  selectedTercantumProdukItem = itemTercantumProduk;
                  setState(() {});
                },

                child: DropdownItemCustom(
                  formItem: itemTercantumProduk,
                  isItemSelected: selectedTercantumProdukItem?.id == itemTercantumProduk.id,
                ),
              );
            }).toList(),
          ),
        )
      ],
    );
  }

  Widget formTercantumKemasan(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // Text Tipe Lokasi Pengawasan
        Text(
          'Apakah LTHE tercantum pada kotak kemasan?',
          style: blackTextStyle.copyWith(
            fontSize: 15,
            fontWeight: semiBold,
          ),
        ),

        const SizedBox(height: 16),

        DropdownCustom(
          dropdownText: selectedTercantumKemasanItem?.text,
          child: Column(
            children: listTercantumKemasan.map((itemTercantumKemasan){
              return GestureDetector(
                onTap: (){
                  selectedTercantumKemasanItem = itemTercantumKemasan;
                  setState(() {});
                },

                child: DropdownItemCustom(
                  formItem: itemTercantumKemasan,
                  isItemSelected: selectedTercantumKemasanItem?.id == itemTercantumKemasan.id,
                ),
              );
            }).toList(),
          ),
        )
      ],
    );
  }

  Widget formVisibilitasLTHE(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // Text Tipe Lokasi Pengawasan
        Text(
          'Visibilitas LTHE',
          style: blackTextStyle.copyWith(
            fontSize: 15,
            fontWeight: semiBold,
          ),
        ),

        const SizedBox(height: 16),

        DropdownCustom(
          dropdownText: selectedVisibilitasItem?.text,
          child: Column(
            children: listVisibilitasLTHE.map((itemVisibilitas){
              return GestureDetector(
                onTap: (){
                  selectedVisibilitasItem = itemVisibilitas;
                  setState(() {});
                },

                child: DropdownItemCustom(
                  formItem: itemVisibilitas,
                  isItemSelected: selectedVisibilitasItem?.id == itemVisibilitas.id,
                ),
              );
            }).toList(),
          ),
        )
      ],
    );
  }

  Widget formVisualLTHE(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // Text Tipe Lokasi Pengawasan
        Text(
          'Kesesuain Visual LTHE',
          style: blackTextStyle.copyWith(
            fontSize: 15,
            fontWeight: semiBold,
          ),
        ),

        const SizedBox(height: 16),

        DropdownCustom(
          dropdownText: selectedVisualItem?.text,
          child: Column(
            children: listKesesuaianVisual.map((itemVisual){
              return GestureDetector(
                onTap: (){
                  selectedVisualItem = itemVisual;
                  setState(() {});
                },

                child: DropdownItemCustom(
                  formItem: itemVisual,
                  isItemSelected: selectedVisualItem?.id == itemVisual.id,
                ),
              );
            }).toList(),
          ),
        )
      ],
    );
  }
}
