// Kapasitas pengenal
import '../../../../models/form_item_model.dart';

List<FormItemModel> listKapasitasPengenal = [
  FormItemModel(id: 1, text: '1/2'),
  FormItemModel(id: 2, text: '3/4'),
  FormItemModel(id: 3, text: '1'),
  FormItemModel(id: 4, text: '1,5'),
  FormItemModel(id: 5, text: '2'),
  FormItemModel(id: 6, text: '2,5'),
  FormItemModel(id: 7, text: '3'),
];

// Teknologi
List<FormItemModel> listTeknologi = [
  FormItemModel(id: 1, text: 'Standar'),
  FormItemModel(id: 2, text: 'Fixed Speed'),
  FormItemModel(id: 3, text: 'Inverter'),
];