import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:skem/layouts/custom_widget/page_layout/blue_linear_background.dart';

import '../../../../cubit/form_ui_cubit/image_picker/image_picker_cubit.dart';
import '../../../../layouts/custom_widget/buttons/filled_button_custom.dart';
import '../../../../layouts/custom_widget/form/foto_lokasi_form_custom.dart';
import '../../../../layouts/custom_widget/form/inspeksi_input_custom.dart';
import '../../../../shared/shared_method/img_picker.dart';
import '../../../../shared/shared_theme/fonts.dart';

class FormRitelBaruLTHEScreen extends StatefulWidget {
  const FormRitelBaruLTHEScreen({super.key});

  @override
  State<FormRitelBaruLTHEScreen> createState() => _FormRitelBaruLTHEScreenState();
}

class _FormRitelBaruLTHEScreenState extends State<FormRitelBaruLTHEScreen> {
  final ImagePickerCubit _imgPickerCubit = ImagePickerCubit();

  void pickImage(){
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20),
          ),
          title: Text('Pilih Sumber Foto'),

          content: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              IconButton(
                icon: Icon(
                  Icons.camera_alt,
                ),
                iconSize: 50,
                onPressed: () async {
                  final image = await selectImageCamera();
                  if(image != null){
                    final imgPath = image.path;

                    if(imgPath != ''){
                      _imgPickerCubit.setImgSource(imgPath);
                    }

                    Navigator.pop(context);
                  }
                },
              ),

              IconButton(
                icon: Icon(
                  Icons.photo_library,
                ),
                iconSize: 50,
                onPressed: () async {
                  final image = await selectImageGallery();
                  if(image != null){
                    final imgPath = image.path;

                    if(imgPath != ''){
                      _imgPickerCubit.setImgSource(imgPath);
                    }

                    Navigator.pop(context);
                  }
                },
              ),
            ],
          ),
          actions:[
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text('Cancel'),
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.white,
        elevation: 0.6,
        // Icon Arrow Back
        leading: IconButton(
          onPressed: (){
            Navigator.pop(context);
          },

          icon: const Icon(
            Icons.arrow_back,
            size: 26,
            color: Colors.black,
          ),
        ),

        // Page Title
        title: Text(
          'Data Ritel Baru',
          style: blackTextStyle.copyWith(
            fontSize: 20,
            fontWeight: bold,
          ),
        ),
      ),

      body: BlueLinearBackground(
        middleStop: 0.2,
        child: Stack(
          children: [
            // Icon Kuning Kanan Bawah
            Positioned(
              bottom: -25,
              right: -55,

              child: SvgPicture.asset(
                'assets/icon/svg_circle_kanan_bawah.svg',
                width: 220,
                height: 220,
              ),
            ),

            // Main Page Content
            ListView(
              padding: const EdgeInsets.all(20),
              children: [
                // Section Form Foto Lokasi Pengawasan
                formFotoLTHE(),

                const SizedBox(height: 25),

                // Section Form Detail Ritel Baru
                formDetailRitelBaru(),

                const SizedBox(height: 25),

                // Batalkan & Lanjutkan Button
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    // Batalkan
                    FilledButtonCustom(
                      width: MediaQuery.sizeOf(context).width * 0.38,
                      height: 56,
                      buttonColor: const Color(0xffEB5757),
                      label: Text(
                        'Kembali',
                        style: whiteTextStyle.copyWith(
                            fontSize: 16,
                            fontWeight: bold
                        ),
                      ),

                      onTap: (){
                        Navigator.pop(context);
                      },
                    ),

                    // Batalkan
                    FilledButtonCustom(
                      width: MediaQuery.sizeOf(context).width * 0.45,
                      height: 56,
                      buttonColor: const Color(0xff04AFD2),
                      label: Text(
                        'Lanjutkan',
                        style: whiteTextStyle.copyWith(
                            fontSize: 16,
                            fontWeight: bold
                        ),
                      ),

                      onTap: (){
                        Navigator.pushNamed(context, '/form-compliance-lainnya-udara');
                      },
                    ),
                  ],
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget formFotoLTHE(){
    return BlocBuilder<ImagePickerCubit, String?>(
      bloc: _imgPickerCubit,
      builder: (context, state){
        return FotoLokasiFormCustom(
          imgSrc: state,
          onTap: (){
            pickImage();
          },
        );
      },
    );
  }

  Widget formDetailRitelBaru(){
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.all(16),

      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadiusDirectional.circular(10),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.1),
            offset: const Offset(0, 4),
            blurRadius: 10,
          ),
        ],
      ),

      child: const Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // Nomor SHE
          InspeksiInputCustom(
            formLabel: 'Nomor SHE',
            hintText: 'Isikan nomor SHE',
          ),

          SizedBox(height: 16),

          // Jumlah Bintang
          InspeksiInputCustom(
            formLabel: 'Jumlah Bintang',
            hintText: 'Isikan jumlah bintang',
          ),

          SizedBox(height: 16),

          // Nilai Efisiensi Energi (EER), CSPF
          InspeksiInputCustom(
            formLabel: 'Nilai Efisiensi Energi (EER), CSPF',
            hintText: 'Isikan nilai EER, CSPF',
          ),

          SizedBox(height: 16),

          // Model Unit Dalam
          InspeksiInputCustom(
            formLabel: 'Model Unit Dalam',
            hintText: 'Isikan model unit dalam',
          ),

          SizedBox(height: 16),

          // Model Unit Luar
          InspeksiInputCustom(
            formLabel: 'Model Unit Luar',
            hintText: 'Isikan model unit luar',
          ),

          SizedBox(height: 16),

          // Daya
          InspeksiInputCustom(
            formLabel: 'Daya',
            hintText: 'Isikan daya',
          ),

          SizedBox(height: 16),

          // Kapasitas Pendinginan
          InspeksiInputCustom(
            formLabel: 'Kapasitas Pendinginan',
            hintText: 'Isikan kapasitas pendinginan',
          ),
        ],
      ),
    );
  }
}
