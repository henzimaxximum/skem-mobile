import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:skem/models/form_item_model.dart';
import 'package:skem/ui_pages/inspeksi_page/pengondisi_udara/compliance_lainnya_udara/compliance_lainnya_questions.dart';

import '../../../../layouts/custom_widget/buttons/filled_button_custom.dart';
import '../../../../layouts/custom_widget/form/dropdown_custom.dart';
import '../../../../layouts/custom_widget/form/dropdown_item_custom.dart';
import '../../../../layouts/custom_widget/page_layout/blue_linear_background.dart';
import '../../../../layouts/custom_widget/form/inspeksi_form_card.dart';
import '../../../../shared/shared_theme/fonts.dart';

class FormComplianceLainnyaScreen extends StatefulWidget {
  const FormComplianceLainnyaScreen({super.key});

  @override
  State<FormComplianceLainnyaScreen> createState() => _FormComplianceLainnyaScreenState();
}

class _FormComplianceLainnyaScreenState extends State<FormComplianceLainnyaScreen> {
  FormItemModel? selectedTercantumSNI;
  FormItemModel? selectedTercantumEkolabel;
  FormItemModel? selectedTercantumGaransi;
  FormItemModel? selectedProdukBahasIndo;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.white,
        elevation: 0.6,
        // Icon Arrow Back
        leading: IconButton(
          onPressed: (){
            Navigator.pop(context);
          },

          icon: const Icon(
            Icons.arrow_back,
            size: 26,
            color: Colors.black,
          ),
        ),

        // Page Title
        title: Text(
          'Compliance Regulasi Lainnya',
          style: blackTextStyle.copyWith(
            fontSize: 20,
            fontWeight: bold,
          ),
        ),
      ),

      body: BlueLinearBackground(
        middleStop: 0.2,
        child: Stack(
          children: [
            // Icon Kuning Kanan Bawah
            Positioned(
              bottom: -25,
              right: -55,

              child: SvgPicture.asset(
                'assets/icon/svg_circle_kanan_bawah.svg',
                width: 220,
                height: 220,
              ),
            ),

            // Main Page Content
            ListView(
              padding: const EdgeInsets.all(20),
              children: [
                // Form Content
                InspeksiFormCard(
                  child: Column(
                    children: [
                      // Dropdown Tercantum SNI
                      formTercantumSNI(),

                      const SizedBox(height: 25),

                      // Dropdown Tercantum Ekolabel
                      formTercantumEkolabel(),

                      const SizedBox(height: 25),

                      // Dropdown Tercantum Garansi
                      formTercantumGaransi(),

                      const SizedBox(height: 25),

                      // Dropdown Apakah Bahasa Indonesia
                      formBahasaIndonesia(),
                    ],
                  ),
                ),

                const SizedBox(height: 25),

                // Simpan-Baru & Simpan-Selesai Button
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    // Simpan & Data Baru Button
                    FilledButtonCustom(
                      width: MediaQuery.sizeOf(context).width * 0.55,
                      height: 70,
                      buttonColor: const Color(0xff04AFD2),
                      label: Text(
                        'Simpan dan Input Data Baru',
                        textAlign: TextAlign.center,
                        style: whiteTextStyle.copyWith(
                            fontSize: 16,
                            fontWeight: bold
                        ),
                      ),

                      onTap: (){

                      },
                    ),

                    const SizedBox(height: 10),

                    // Lanjutkan Button
                    FilledButtonCustom(
                      width: MediaQuery.sizeOf(context).width * 0.38,
                      height: 70,
                      buttonColor: const Color(0xff04AFD2),
                      label: Text(
                        'Simpan dan Lanjutkan',
                        textAlign: TextAlign.center,
                        style: whiteTextStyle.copyWith(
                            fontSize: 16,
                            fontWeight: bold
                        ),
                      ),

                      onTap: (){

                      },
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget formTercantumSNI(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // Text SNI Tercantum
        Text(
          'Apakah label SNI tercantum dan dapat terbaca jelas?',
          style: blackTextStyle.copyWith(
            fontSize: 15,
            fontWeight: semiBold,
          ),
        ),

        const SizedBox(height: 16),

        DropdownCustom(
          dropdownText: selectedTercantumSNI?.text,
          child: Column(
            children: listSNITercantum.map((itemTercantumProduk){
              return GestureDetector(
                onTap: (){
                  selectedTercantumSNI = itemTercantumProduk;
                  setState(() {});
                },

                child: DropdownItemCustom(
                  formItem: itemTercantumProduk,
                  isItemSelected: selectedTercantumSNI?.id == itemTercantumProduk.id,
                ),
              );
            }).toList(),
          ),
        )
      ],
    );
  }

  Widget formTercantumEkolabel(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // Text Ekolabel Tercantum
        Text(
          'Apakah Ekolabel tercantum dan dapat terbaca jelas?',
          style: blackTextStyle.copyWith(
            fontSize: 15,
            fontWeight: semiBold,
          ),
        ),

        const SizedBox(height: 16),

        DropdownCustom(
          dropdownText: selectedTercantumEkolabel?.text,
          child: Column(
            children: listEkolabelTercantum.map((itemTercantumProduk){
              return GestureDetector(
                onTap: (){
                  selectedTercantumEkolabel = itemTercantumProduk;
                  setState(() {});
                },

                child: DropdownItemCustom(
                  formItem: itemTercantumProduk,
                  isItemSelected: selectedTercantumEkolabel?.id == itemTercantumProduk.id,
                ),
              );
            }).toList(),
          ),
        )
      ],
    );
  }

  Widget formTercantumGaransi(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // Text Garansi Tercantum
        Text(
          'Apakah kartu garansi tercantum dan dapat terbaca jelas?',
          style: blackTextStyle.copyWith(
            fontSize: 15,
            fontWeight: semiBold,
          ),
        ),

        const SizedBox(height: 16),

        DropdownCustom(
          dropdownText: selectedTercantumGaransi?.text,
          child: Column(
            children: listKartuGaransiTercantum.map((itemTercantumProduk){
              return GestureDetector(
                onTap: (){
                  selectedTercantumGaransi = itemTercantumProduk;
                  setState(() {});
                },

                child: DropdownItemCustom(
                  formItem: itemTercantumProduk,
                  isItemSelected: selectedTercantumGaransi?.id == itemTercantumProduk.id,
                ),
              );
            }).toList(),
          ),
        )
      ],
    );
  }

  Widget formBahasaIndonesia(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // Text Apakah Bahasa Indonesia
        Text(
          'Apakah produk dalam bahasa indonesia?',
          style: blackTextStyle.copyWith(
            fontSize: 15,
            fontWeight: semiBold,
          ),
        ),

        const SizedBox(height: 16),

        DropdownCustom(
          dropdownText: selectedProdukBahasIndo?.text,
          child: Column(
            children: listPordukBahasaIndonesia.map((itemTercantumProduk){
              return GestureDetector(
                onTap: (){
                  selectedProdukBahasIndo = itemTercantumProduk;
                  setState(() {});
                },

                child: DropdownItemCustom(
                  formItem: itemTercantumProduk,
                  isItemSelected: selectedProdukBahasIndo?.id == itemTercantumProduk.id,
                ),
              );
            }).toList(),
          ),
        )
      ],
    );
  }
}
