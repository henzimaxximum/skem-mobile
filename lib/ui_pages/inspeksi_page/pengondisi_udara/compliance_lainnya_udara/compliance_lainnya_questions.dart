import '../../../../models/form_item_model.dart';

// Apakah label SNI tercantum dan dapat terbaca jelas
List<FormItemModel> listSNITercantum = [
  FormItemModel(id: 1, text: 'Ya'),
  FormItemModel(id: 2, text: 'Tidak Yakin'),
  FormItemModel(id: 3, text: 'Tidak'),
];

// Apakah Ekolabel tercantum dan dapat terbaca jelas
List<FormItemModel> listEkolabelTercantum = [
  FormItemModel(id: 1, text: 'Ya'),
  FormItemModel(id: 2, text: 'Tidak Yakin'),
  FormItemModel(id: 3, text: 'Tidak'),
];

// Apakah kartu garansi tercantum dan dapat terbaca jelas
List<FormItemModel> listKartuGaransiTercantum = [
  FormItemModel(id: 1, text: 'Ya'),
  FormItemModel(id: 2, text: 'Tidak Yakin'),
  FormItemModel(id: 3, text: 'Tidak'),
];

// Apakah produk dalam bahasa indonesia
List<FormItemModel> listPordukBahasaIndonesia = [
  FormItemModel(id: 1, text: 'Ya'),
  FormItemModel(id: 2, text: 'Tidak Yakin'),
  FormItemModel(id: 3, text: 'Tidak'),
];