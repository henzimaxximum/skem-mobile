import '../../../../models/form_item_model.dart';

// Tipe Penanak Nasi
List<FormItemModel> listTipePenanakNasi = [
  FormItemModel(id: 1, text: 'IH'),
  FormItemModel(id: 2, text: 'Pressure IH'),
  FormItemModel(id: 3, text: 'Gas'),
  FormItemModel(id: 4, text: 'Microcomputer'),
];