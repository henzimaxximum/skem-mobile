import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:skem/layouts/custom_widget/buttons/category_button_custom.dart';
import 'package:skem/layouts/custom_widget/page_layout/blue_linear_background.dart';
import 'package:skem/layouts/custom_widget/page_layout/top_bar_custom.dart';
import 'package:skem/shared/shared_values/image_icon_variable.dart';

import '../../cubit/page_controller/page_controller_cubit.dart';
import '../../shared/shared_theme/colors.dart';

class InspeksiScreen extends StatelessWidget {
  const InspeksiScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final PageControllerCubit pageControllerCubit = BlocProvider.of<PageControllerCubit>(context);
    return PopScope(
      canPop: false,
      onPopInvoked : (didPop){
        if(didPop){
          return;
        }
        pageControllerCubit.changePage(0);
      },

      child: Scaffold(
        body: SafeArea(
          child: BlueLinearBackground(
            middleStop: 0.7,
            child: Stack(
              children: [
                // Icon Kuning Kiri Atas
                Positioned(
                  top: -15,
                  left: -45,
                  child: SvgPicture.asset(
                    'assets/icon/svg_circle_kiri_atas.svg',
                    width: 170,
                    height: 170,
                  ),
                ),
          
                // Icon Kuning Kanan Bawah
                Positioned(
                  bottom: -25,
                  right: -55,

                  child: SvgPicture.asset(
                    'assets/icon/svg_circle_kanan_bawah.svg',
                    width: 220,
                    height: 220,
                  ),
                ),
          
                // Main Page Content
                ListView(
                  padding: const EdgeInsets.all(22),
                  children: [
                    // Top Bar
                    TopBarCustom(
                      title: 'Kategori Produk',
                      marginTop: 0,
                      marginBottom: 40,
                      onTap: (){
                        pageControllerCubit.changePage(0);
                      },
                    ),

                    // Section Kategori Produk
                    inspeksiVisualSection(context),

                    const SizedBox(height: 30),

                    // Pemeriksaan Sampel Uji Section
                    sampelUjiSection(context),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget inspeksiVisualSection(BuildContext context){
    return Column(
      children: [
        // Text Kategori Produk
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            // Text Kategori
            Text(
              'Inspeksi Visual',
              style: GoogleFonts.notoSans(
                fontSize: 15,
                fontWeight: FontWeight.w600,
                color: const Color(0xff0F2851),
              ),
            ),

            const SizedBox(width: 10),

            // Grey Line
            Expanded(
              child: Container(
                height: 1,
                color: const Color(0xffC2C2C7),
              ),
            )
          ],
        ),

        const SizedBox(height: 16),

        // Baris 1
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            CategoryButtonCustom(
              iconSrc: iconAc,
              namaKategori: 'Produk\nAC',
              onTap: (){
                Navigator.pushNamed(context, '/form-ritel-udara');
              },
            ),

            CategoryButtonCustom(
              iconSrc: iconKipasAngin,
              namaKategori: 'Produk\nKipas Angin',
              onTap: (){
                Navigator.pushNamed(context, '/form-ritel-angin');
              },
            ),

            CategoryButtonCustom(
              iconSrc: iconLampu,
              namaKategori: 'Produk\nLampu',
              onTap: (){
                Navigator.pushNamed(context, '/form-ritel-lampu');
              },
            ),
          ],
        ),

        const SizedBox(height: 40),

        // Baris 2
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            CategoryButtonCustom(
              iconSrc: iconKulkas,
              namaKategori: 'Produk\nKulkas',
              onTap: (){
                Navigator.pushNamed(context, '/form-ritel-kulkas');
              },
            ),

            CategoryButtonCustom(
              iconSrc: iconPenanakNasi,
              namaKategori: 'Produk\nPenanak Nasi',
              onTap: (){
                Navigator.pushNamed(context, '/form-ritel-nasi');
              },
            ),
            const SizedBox(
              width: 60,
            ),
          ],
        ),
      ],
    );
  }

  Widget sampelUjiSection(BuildContext context){
    return Column(
      children: [
        // Text Kategori Produk
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            // Text Kategori
            Text(
              'Pemeriksaan Sampel Uji',
              style: GoogleFonts.notoSans(
                fontSize: 15,
                fontWeight: FontWeight.w600,
                color: const Color(0xff0F2851),
              ),
            ),

            const SizedBox(width: 10),

            // Grey Line
            Expanded(
              child: Container(
                height: 1,
                color: const Color(0xffC2C2C7),
              ),
            ),
          ],
        ),

        const SizedBox(height: 16),

        // Baris 1
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                // Icon Kategori
                GestureDetector(
                  onTap: (){
                    Navigator.pushNamed(context, '/form-informasi-umum-2');
                  },
                  child: Container(
                    width: 60,
                    height: 60,

                    decoration: BoxDecoration(
                      color: lightBlueBackgroundColor,
                      borderRadius: BorderRadius.circular(10),
                    ),

                    child: const Center(
                      child: Icon(
                        Icons.add_chart_rounded,
                        size: 32,
                      ),
                    ),
                  ),
                ),

                const SizedBox(height: 8),

                // Text Kategori
                Text(
                  'Form 2',
                  textAlign: TextAlign.center,
                  style: GoogleFonts.notoSans(
                    fontSize: 12,
                    fontWeight: FontWeight.w600,
                    color: const Color(0xff1C274C),
                  ),
                ),
              ],
            )
          ],
        ),
      ],
    );
  }
}
