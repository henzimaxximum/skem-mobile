import '../../../../models/form_item_model.dart';

// Tipe Kipas Angin
List<FormItemModel> listTipeKipasAngin = [
  FormItemModel(id: 1, text: 'Kipas Meja'),
  FormItemModel(id: 2, text: 'Kipas Berdiri'),
  FormItemModel(id: 3, text: 'Kipas Dinding'),
  FormItemModel(id: 4, text: 'Kipas Kombinasi'),
  FormItemModel(id: 5, text: 'Kipas Profesional'),
];