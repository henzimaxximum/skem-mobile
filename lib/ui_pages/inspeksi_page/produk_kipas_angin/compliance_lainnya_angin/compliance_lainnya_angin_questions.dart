import 'package:skem/models/form_item_model.dart';

// Apakah label SNI tercantum dan dapat terbaca jelas
List<FormItemModel> listSNITercantumAngin = [
  FormItemModel(id: 1, text: 'Ya'),
  FormItemModel(id: 2, text: 'Tidak Yakin'),
  FormItemModel(id: 3, text: 'Tidak'),
];

// Apakah Ekolabel tercantum dan dapat terbaca jelas
List<FormItemModel> listEkolabelTercantumAngin = [
  FormItemModel(id: 1, text: 'Ya'),
  FormItemModel(id: 2, text: 'Tidak Yakin'),
  FormItemModel(id: 3, text: 'Tidak'),
];

// Apakah kartu garansi tercantum dan dapat terbaca jelas
List<FormItemModel> listKartuGaransiTercantumAngin = [
  FormItemModel(id: 1, text: 'Ya'),
  FormItemModel(id: 2, text: 'Tidak Yakin'),
  FormItemModel(id: 3, text: 'Tidak'),
];

// Apakah produk dalam bahasa indonesia
List<FormItemModel> listPordukBahasaIndonesiaAngin = [
  FormItemModel(id: 1, text: 'Ya'),
  FormItemModel(id: 2, text: 'Tidak Yakin'),
  FormItemModel(id: 3, text: 'Tidak'),
];