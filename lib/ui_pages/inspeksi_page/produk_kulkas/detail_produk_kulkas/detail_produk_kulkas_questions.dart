import '../../../../models/form_item_model.dart';

// Tipe Kulkas
List<FormItemModel> listTipeKulkas = [
  FormItemModel(id: 1, text: '1 Pintu'),
  FormItemModel(id: 2, text: '2 Pintu (Top Freezer)'),
  FormItemModel(id: 3, text: '2 Pintu (Bottom Freezer)'),
  FormItemModel(id: 4, text: 'Side-by-side'),
  FormItemModel(id: 5, text: 'French & Multi-door'),
];