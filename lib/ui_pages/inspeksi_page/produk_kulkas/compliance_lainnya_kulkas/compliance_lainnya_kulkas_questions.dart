import 'package:skem/models/form_item_model.dart';

// Apakah label SNI tercantum dan dapat terbaca jelas
List<FormItemModel> listSNITercantumKulkas = [
  FormItemModel(id: 1, text: 'Ya'),
  FormItemModel(id: 2, text: 'Tidak Yakin'),
  FormItemModel(id: 3, text: 'Tidak'),
];

// Apakah Ekolabel tercantum dan dapat terbaca jelas
List<FormItemModel> listEkolabelTercantumKulkas = [
  FormItemModel(id: 1, text: 'Ya'),
  FormItemModel(id: 2, text: 'Tidak Yakin'),
  FormItemModel(id: 3, text: 'Tidak'),
];

// Apakah kartu garansi tercantum dan dapat terbaca jelas
List<FormItemModel> listKartuGaransiTercantumKulkas = [
  FormItemModel(id: 1, text: 'Ya'),
  FormItemModel(id: 2, text: 'Tidak Yakin'),
  FormItemModel(id: 3, text: 'Tidak'),
];

// Apakah produk dalam bahasa indonesia
List<FormItemModel> listPordukBahasaIndonesiaKulkas = [
  FormItemModel(id: 1, text: 'Ya'),
  FormItemModel(id: 2, text: 'Tidak Yakin'),
  FormItemModel(id: 3, text: 'Tidak'),
];