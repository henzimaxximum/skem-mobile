import '../../../../models/form_item_model.dart';

// Apakah tenaga penjual familiar dengan LTHE
List<FormItemModel> listFamiliarLTHEKulkas = [
  FormItemModel(id: 1, text: 'Ya'),
  FormItemModel(id: 2, text: 'Sedikit'),
  FormItemModel(id: 3, text: 'Tidak'),
];

// Seberapa seringkah tenaga penjual menemukan produk tanpa LTHE
List<FormItemModel> listMenemukanTanpaLTHEKulkas = [
  FormItemModel(id: 1, text: 'Tidak Pernah'),
  FormItemModel(id: 2, text: 'Jarang'),
  FormItemModel(id: 3, text: 'Sering'),
  FormItemModel(id: 4, text: 'Sangat Sering'),
];

// Apakah konsumen menanyakan tentang LTHE
List<FormItemModel> listMenanyakanLTHEKulkas = [
  FormItemModel(id: 1, text: 'Ya'),
  FormItemModel(id: 2, text: 'Sering'),
  FormItemModel(id: 3, text: 'Kadang-kadang'),
  FormItemModel(id: 4, text: 'Tidak Pernah'),
];

// Apakah pramuniaga lokasi pengawasan dapat menjelaskan arti LTHE dengan benar
List<FormItemModel> listMenjelaskanLTHEKulkas = [
  FormItemModel(id: 1, text: 'Ya'),
  FormItemModel(id: 2, text: 'Sebagian'),
  FormItemModel(id: 3, text: 'Tidak'),
];