import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:skem/ui_pages/inspeksi_page/produk_kulkas/pengalaman_lthe_kulkas/pengalaman_lthe_kulkas_questions.dart';

import '../../../../layouts/custom_widget/buttons/filled_button_custom.dart';
import '../../../../layouts/custom_widget/form/dropdown_custom.dart';
import '../../../../layouts/custom_widget/form/dropdown_item_custom.dart';
import '../../../../layouts/custom_widget/page_layout/blue_linear_background.dart';
import '../../../../layouts/custom_widget/form/inspeksi_form_card.dart';
import '../../../../models/form_item_model.dart';
import '../../../../shared/shared_theme/fonts.dart';

class FormPengalamanLTHEKulkasScreen extends StatefulWidget {
  const FormPengalamanLTHEKulkasScreen({super.key});

  @override
  State<FormPengalamanLTHEKulkasScreen> createState() => _FormPengalamanLTHEKulkasScreenState();
}

class _FormPengalamanLTHEKulkasScreenState extends State<FormPengalamanLTHEKulkasScreen> {
  FormItemModel? selectedFamiliarItem;
  FormItemModel? selectedMenemukanItem;
  FormItemModel? selectedMenanyakanItem;
  FormItemModel? selectedMenjelaskanItem;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.white,
        elevation: 0.6,
        // Icon Arrow Back
        leading: IconButton(
          onPressed: (){
            Navigator.pop(context);
          },

          icon: const Icon(
            Icons.arrow_back,
            size: 26,
            color: Colors.black,
          ),
        ),

        // Page Title
        title: Text(
          'Pengalaman Terhadap LTHE',
          style: blackTextStyle.copyWith(
            fontSize: 20,
            fontWeight: bold,
          ),
        ),
      ),

      body: BlueLinearBackground(
        middleStop: 0.2,
        child: Stack(
          children: [
            // Icon Kuning Kiri Atas
            Positioned(
              top: -25,
              left: -55,

              child: SvgPicture.asset(
                'assets/icon/svg_circle_kiri_atas.svg',
                width: 220,
                height: 220,
              ),
            ),

            // Icon Kuning Kanan Bawah
            Positioned(
              bottom: -25,
              right: -55,

              child: SvgPicture.asset(
                'assets/icon/svg_circle_kanan_bawah.svg',
                width: 220,
                height: 220,
              ),
            ),

            // Main Page Content
            ListView(
              padding: const EdgeInsets.all(20),
              children: [
                // Form Content
                InspeksiFormCard(
                  child: Column(
                    children: [
                      formFamiliarLTHE(),

                      const SizedBox(height: 28),

                      formMenemukanLTHE(),

                      const SizedBox(height: 28),

                      formMenanyakanLTHE(),

                      const SizedBox(height: 28),

                      formMenjelaskanLTHE(),
                    ],
                  ),
                ),

                const SizedBox(height: 40),

                // Kembali & Lanjutkan Button
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    // Batalkan
                    Flexible(
                      child: FilledButtonCustom(
                        width: MediaQuery.sizeOf(context).width * 0.38,
                        height: 56,
                        buttonColor: const Color(0xffEB5757),
                        label: Text(
                          'Kembali',
                          style: whiteTextStyle.copyWith(
                              fontSize: 16,
                              fontWeight: bold
                          ),
                        ),

                        onTap: (){
                          Navigator.pop(context);
                        },
                      ),
                    ),

                    // Lanjutkan
                    Flexible(
                      child: FilledButtonCustom(
                        width: MediaQuery.sizeOf(context).width * 0.45,
                        height: 56,
                        buttonColor: const Color(0xff04AFD2),
                        label: Text(
                          'Lanjutkan',
                          style: whiteTextStyle.copyWith(
                              fontSize: 16,
                              fontWeight: bold
                          ),
                        ),

                        onTap: (){
                          Navigator.pushNamed(context, '/form-detail-produk-kulkas');
                        },
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget formFamiliarLTHE(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // Text Tipe Lokasi Pengawasan
        Text(
          'Apakah tenaga penjual familiar dengan LTHE?',
          style: blackTextStyle.copyWith(
            fontSize: 15,
            fontWeight: semiBold,
          ),
        ),

        const SizedBox(height: 16),

        DropdownCustom(
          dropdownText: selectedFamiliarItem?.text,
          child: Column(
            children: listFamiliarLTHEKulkas.map((itemFamiliar){
              return GestureDetector(
                onTap: (){
                  selectedFamiliarItem = itemFamiliar;
                  setState(() {});
                },

                child: DropdownItemCustom(
                  formItem: itemFamiliar,
                  isItemSelected: selectedFamiliarItem?.id == itemFamiliar.id,
                ),
              );
            }).toList(),
          ),
        )
      ],
    );
  }

  Widget formMenemukanLTHE(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // Text Tipe Lokasi Pengawasan
        Text(
          'Seberapa seringkah tenaga penjual menemukan produk tanpa LTHE?',
          style: blackTextStyle.copyWith(
            fontSize: 15,
            fontWeight: semiBold,
          ),
        ),

        const SizedBox(height: 16),

        DropdownCustom(
          dropdownText: selectedMenemukanItem?.text,
          child: Column(
            children: listMenemukanTanpaLTHEKulkas.map((itemMenemukan){
              return GestureDetector(
                onTap: (){
                  selectedMenemukanItem = itemMenemukan;
                  setState(() {});
                },

                child: DropdownItemCustom(
                  formItem: itemMenemukan,
                  isItemSelected: selectedMenemukanItem?.id == itemMenemukan.id,
                ),
              );
            }).toList(),
          ),
        )
      ],
    );
  }

  Widget formMenanyakanLTHE(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // Text Tipe Lokasi Pengawasan
        Text(
          'Apakah konsumen menanyakan tentang LTHE?',
          style: blackTextStyle.copyWith(
            fontSize: 15,
            fontWeight: semiBold,
          ),
        ),

        const SizedBox(height: 16),

        DropdownCustom(
          dropdownText: selectedMenanyakanItem?.text,
          child: Column(
            children: listMenanyakanLTHEKulkas.map((itemMenanyakan){
              return GestureDetector(
                onTap: (){
                  selectedMenanyakanItem = itemMenanyakan;
                  setState(() {});
                },

                child: DropdownItemCustom(
                  formItem: itemMenanyakan,
                  isItemSelected: selectedMenanyakanItem?.id == itemMenanyakan.id,
                ),
              );
            }).toList(),
          ),
        )
      ],
    );
  }

  Widget formMenjelaskanLTHE(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // Text Tipe Lokasi Pengawasan
        Text(
          'Apakah pramuniaga lokasi pengawasan dapat menjelaskan arti LTHE dengan benar?',
          style: blackTextStyle.copyWith(
            fontSize: 15,
            fontWeight: semiBold,
          ),
        ),

        const SizedBox(height: 16),

        DropdownCustom(
          dropdownText: selectedMenjelaskanItem?.text,
          child: Column(
            children: listMenjelaskanLTHEKulkas.map((itemMenjelaskan){
              return GestureDetector(
                onTap: (){
                  selectedMenjelaskanItem = itemMenjelaskan;
                  setState(() {});
                },

                child: DropdownItemCustom(
                  formItem: itemMenjelaskan,
                  isItemSelected: selectedMenjelaskanItem?.id == itemMenjelaskan.id,
                ),
              );
            }).toList(),
          ),
        )
      ],
    );
  }
}
