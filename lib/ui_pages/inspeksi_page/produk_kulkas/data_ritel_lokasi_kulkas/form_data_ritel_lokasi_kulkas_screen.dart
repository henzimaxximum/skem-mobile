import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:skem/ui_pages/inspeksi_page/produk_kulkas/data_ritel_lokasi_kulkas/data_ritel_lokasi_kulkas_questions.dart';

import '../../../../cubit/form_ui_cubit/image_picker/image_picker_cubit.dart';
import '../../../../cubit/form_ui_cubit/location_picker/location_picker_cubit.dart';
import '../../../../layouts/custom_widget/buttons/filled_button_custom.dart';
import '../../../../layouts/custom_widget/form/denah_lokasi_form_custom.dart';
import '../../../../layouts/custom_widget/form/dropdown_custom.dart';
import '../../../../layouts/custom_widget/form/dropdown_item_custom.dart';
import '../../../../layouts/custom_widget/form/foto_lokasi_form_custom.dart';
import '../../../../layouts/custom_widget/form/inspeksi_input_custom.dart';
import '../../../../layouts/custom_widget/page_layout/blue_linear_background.dart';
import '../../../../layouts/custom_widget/form/map_placeholder_widget.dart';
import '../../../../models/form_item_model.dart';
import '../../../../shared/shared_method/img_picker.dart';
import '../../../../shared/shared_theme/fonts.dart';

class FormDataRitelLokasiKulkasScreen extends StatefulWidget {
  const FormDataRitelLokasiKulkasScreen({super.key});

  @override
  State<FormDataRitelLokasiKulkasScreen> createState() => _FormDataRitelLokasiKulkasScreenState();
}

class _FormDataRitelLokasiKulkasScreenState extends State<FormDataRitelLokasiKulkasScreen> {
  FormItemModel? selectedItemLokasi;

  final ImagePickerCubit _imgPickerCubit = ImagePickerCubit();
  final LocationPickerCubit _locationPickerCubit = LocationPickerCubit();

  void pickImage(){
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20),
          ),
          title: const Text('Pilih Sumber Foto'),

          content: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              IconButton(
                icon: const Icon(
                  Icons.camera_alt,
                ),
                iconSize: 50,
                onPressed: () async {
                  final image = await selectImageCamera();
                  if(image != null){
                    final imgPath = image.path;

                    if(imgPath != ''){
                      _imgPickerCubit.setImgSource(imgPath);
                    }

                    Navigator.pop(context);
                  }
                },
              ),

              IconButton(
                icon: const Icon(
                  Icons.photo_library,
                ),
                iconSize: 50,
                onPressed: () async {
                  final image = await selectImageGallery();
                  if(image != null){
                    final imgPath = image.path;

                    if(imgPath != ''){
                      _imgPickerCubit.setImgSource(imgPath);
                    }

                    Navigator.pop(context);
                  }
                },
              ),
            ],
          ),
          actions:[
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: const Text('Cancel'),
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0.6,
        centerTitle: true,

        // Icon Arrow Back
        leading: IconButton(
          onPressed: (){
            Navigator.pop(context);
          },

          icon: const Icon(
            Icons.arrow_back,
            size: 26,
            color: Colors.black,
          ),
        ),

        // Page Title
        title: Text(
          'Data Ritel',
          style: blackTextStyle.copyWith(
            fontSize: 20,
            fontWeight: bold,
          ),
        ),
      ),

      body: BlueLinearBackground(
        middleStop: 0.2,
        child: Stack(
          children: [
            // Icon Kuning Kiri Atas
            Positioned(
              top: -25,
              left: -55,

              child: SvgPicture.asset(
                'assets/icon/svg_circle_kiri_atas.svg',
                width: 220,
                height: 220,
              ),
            ),

            // Icon Kuning Kanan Bawah
            Positioned(
              bottom: -25,
              right: -55,

              child: SvgPicture.asset(
                'assets/icon/svg_circle_kanan_bawah.svg',
                width: 220,
                height: 220,
              ),
            ),

            // Main Page Content
            ListView(
              padding: const EdgeInsets.all(20),
              children: [
                // Section Form Tipe Lokasi Pengawasan
                formLokasiPengawasan(),

                const SizedBox(height: 25),

                // Section Form Foto Lokasi Pengawasan
                formFotoLokasiPengawasan(),

                const SizedBox(height: 25),

                // Section Form Denah Lokasi Pengawasan
                formDenahLokasiPengawasan(),

                const SizedBox(height: 25),

                // Section Form Detail Lokasi Pengawasan
                formDetailLokasiPengawasan(),

                const SizedBox(height: 25),

                // Batalkan & Lanjutkan Button
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    // Batalkan
                    FilledButtonCustom(
                      width: MediaQuery.sizeOf(context).width * 0.38,
                      height: 56,
                      buttonColor: const Color(0xffEB5757),
                      label: Text(
                        'Batalkan',
                        style: whiteTextStyle.copyWith(
                            fontSize: 16,
                            fontWeight: bold
                        ),
                      ),

                      onTap: (){
                        Navigator.pop(context);
                      },
                    ),

                    // Batalkan
                    FilledButtonCustom(
                      width: MediaQuery.sizeOf(context).width * 0.45,
                      height: 56,
                      buttonColor: const Color(0xff04AFD2),
                      label: Text(
                        'Lanjutkan',
                        style: whiteTextStyle.copyWith(
                            fontSize: 16,
                            fontWeight: bold
                        ),
                      ),

                      onTap: (){
                        Navigator.pushNamed(context, '/form-pengalaman-lthe-kulkas');
                      },
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget formFotoLokasiPengawasan(){
    return BlocBuilder<ImagePickerCubit, String?>(
      bloc: _imgPickerCubit,
      builder: (context, state){
        return FotoLokasiFormCustom(
          imgSrc: state,
          onTap: (){
            pickImage();
          },
        );
      },
    );
  }

  Widget formDenahLokasiPengawasan(){
    return BlocBuilder<LocationPickerCubit, LocationPickerState>(
      bloc: _locationPickerCubit,
      builder: (context,state){
        // Success State
        if(state is LocationPickerSuccess){
          return DenahLokasiFormCustom(
            userLocationWidget: MapPlaceholderSuccess(address: state.location),

            onTap: () async {
              _locationPickerCubit.getCurrentLocation();
            },
          );
        }

        // Loading State
        if(state is LocationPickerLoading){
          return DenahLokasiFormCustom(
            userLocationWidget: const MapPlaceholderLoading(),

            onTap: () async {},
          );
        }

        // Default State
        return DenahLokasiFormCustom(
          userLocationWidget: const MapPlaceholderDefault(),

          onTap: () async {
            _locationPickerCubit.getCurrentLocation();
          },
        );
      },
    );
  }

  Widget formLokasiPengawasan(){
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.all(16),

      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadiusDirectional.circular(10),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.1),
            offset: const Offset(0, 4),
            blurRadius: 10,
          ),
        ],
      ),

      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // Text Tipe Lokasi Pengawasan
          Text(
            'Tipe Lokasi Pengawasan',
            style: blackTextStyle.copyWith(
              fontSize: 15,
              fontWeight: semiBold,
            ),
          ),

          const SizedBox(height: 16),

          DropdownCustom(
            dropdownText: selectedItemLokasi?.text,
            child: Column(
              children: listTipeLokasiPengawasanKulkas.map((itemLokasi){
                return GestureDetector(
                  onTap: (){
                    selectedItemLokasi = itemLokasi;
                    setState(() {});
                  },

                  child: DropdownItemCustom(
                    formItem: itemLokasi,
                    isItemSelected: selectedItemLokasi?.id == itemLokasi.id,
                  ),
                );
              }).toList(),
            ),
          )
        ],
      ),
    );
  }

  Widget formDetailLokasiPengawasan(){
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.all(16),

      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadiusDirectional.circular(10),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.1),
            offset: const Offset(0, 4),
            blurRadius: 10,
          ),
        ],
      ),

      child: const Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // Nama Lokasi Pengawasan
          InspeksiInputCustom(
            formLabel: 'Nama Lokasi Pengawasan',
            hintText: 'Masukkan Nama Lokasi Pengawasan',
          ),

          SizedBox(height: 16),

          // Alamat Lokasi Pengawasan
          InspeksiInputCustom(
            formLabel: 'Alamat Lokasi Pengawasan',
            hintText: 'Masukkan Alamat Lokasi Pengawasan',
          ),

          SizedBox(height: 16),

          // Wilayah Lokasi Pengawasan
          InspeksiInputCustom(
            formLabel: 'Wilayah Lokasi Pengawasan',
            hintText: 'Masukkan Wilayah lokasi',
          ),

          SizedBox(height: 16),

          // Nama Pengawas
          InspeksiInputCustom(
            formLabel: 'Nama Pengawas',
            hintText: 'Masukkan Nama Pengawas',
          ),
        ],
      ),
    );
  }
}
