
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:skem/shared/shared_theme/colors.dart';
import 'package:skem/shared/shared_theme/fonts.dart';

import '../../cubit/page_controller/page_controller_cubit.dart';

class AkunScreen extends StatelessWidget {
  const AkunScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final PageControllerCubit pageControllerCubit = BlocProvider.of<PageControllerCubit>(context);
    return PopScope(
      canPop: false,
      onPopInvoked : (didPop){
        if(didPop){
          return;
        }
        pageControllerCubit.changePage(0);
      },
      child: Scaffold(
        backgroundColor: lightBackgroundColor,
        appBar: AppBar(
          backgroundColor: lightBackgroundColor,
          centerTitle: true,

          // Arrow Back
          leading: IconButton(
            onPressed: (){
              pageControllerCubit.changePage(0);
            },

            icon: const Padding(
              padding: EdgeInsets.only(left: 10),
              child: Icon(
                Icons.arrow_back_ios,
                size: 25,
              ),
            ),
          ),

          // My Profile Title
          title: Text(
            'My Profile',
            style: blackTextStyle.copyWith(
              fontSize: 20,
              fontWeight: semiBold,
            ),
          ),
        ),

        body: Container(
          width: double.infinity,
          margin: const EdgeInsets.all(20),
          padding: const EdgeInsets.all(24),

          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
          ),

          child: IntrinsicHeight(
            child: Column(
              children: [
                // User Information Section
                userProfileSection(),

                const SizedBox(height: 40),

                // Menu Edit Profile
                editProfileMenu(context),

                const SizedBox(height: 30),

                // Menu Pengaturan Bahasa
                languageSettingMenu(context),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget userProfileSection(){
    return Column(
      children: [
        // User Profile Picture
        Container(
          width: 110,
          height: 110,

          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: lightBlueColor,
          ),

          child: const Center(
            child: Icon(
              Icons.person_outline,
              size: 70,
            ),
          ),
        ),

        const SizedBox(height: 16),

        // User Name
        Text(
          'Jhon Doe',
          style: blackTextStyle.copyWith(
            fontSize: 20,
            fontWeight: bold,
          ),
        ),
      ],
    );
  }

  Widget editProfileMenu(BuildContext context){
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        // Menu Icon
        const Icon(
          Icons.person,
          size: 26,
        ),

        const SizedBox(width: 18),

        // Menu Text
        Text(
          context.tr('akun_screen.edit_profile_menu_label'),
          style: blackTextStyle.copyWith(
            fontSize: 15,
            fontWeight: medium,
          ),
        )
      ],
    );
  }

  Widget languageSettingMenu(BuildContext context){
    return Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: (){
          Navigator.pushNamed(context, '/language-setting');
        },

        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            // Menu Icon
            const Icon(
              Icons.language,
              size: 26,
            ),

            const SizedBox(width: 18),

            // Menu Text
            Text(
              context.tr('akun_screen.language_menu_label'),
              style: blackTextStyle.copyWith(
                fontSize: 15,
                fontWeight: medium,
              ),
            )
          ],
        ),
      ),
    );
  }
}
