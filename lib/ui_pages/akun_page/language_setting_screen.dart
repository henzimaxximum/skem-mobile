import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:skem/shared/shared_theme/fonts.dart';

import '../../shared/shared_theme/colors.dart';

class LanguageSettingScreen extends StatelessWidget {
  const LanguageSettingScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: lightBackgroundColor,
      appBar: AppBar(
        backgroundColor: Colors.white,
        centerTitle: true,
        // Arrow Back
        leading: IconButton(
          onPressed: (){
            Navigator.pop(context);
          },

          icon: const Padding(
            padding: EdgeInsets.only(left: 10),
            child: Icon(
              Icons.arrow_back_ios,
              color: Colors.black,
              size: 25,
            ),
          ),
        ),

        // My Profile Title
        title: Text(
          'Pengaturan Bahasa',
          style: blackTextStyle.copyWith(
            fontSize: 20,
            fontWeight: semiBold,
          ),
        ),
      ),

      body: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            // Bahasa Indonesia
            GestureDetector(
              onTap: () {
                context.setLocale(const Locale('id'));
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(
                    content: Text('snackbar.change_language'.tr()),
                    duration: const Duration(milliseconds: 1300),
                    backgroundColor: Colors.lightGreen,
                  ),
                );
              },

              child: Container(
                width: 140,
                height: 140,
                padding: const EdgeInsets.all(10),

                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Colors.white,
                ),

                child: Center(
                  child: Text(
                    'Bahasa Indonesia',
                    textAlign: TextAlign.center,
                    style: blackTextStyle.copyWith(
                      fontSize: 20,
                      fontWeight: semiBold
                    ),
                  ),
                ),
              ),
            ),

            // Bahasa Inggris
            GestureDetector(
              onTap: () {
                context.setLocale('en_US'.toLocale());
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(
                    content: Text('snackbar.change_language'.tr()),
                    duration: const Duration(milliseconds: 1300),
                    backgroundColor: Colors.lightGreen,
                  ),
                );
              },

              child: Container(
                width: 140,
                height: 140,
                padding: const EdgeInsets.all(10),

                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Colors.white,
                ),

                child: Center(
                  child: Text(
                    'English',
                    textAlign: TextAlign.center,
                    style: blackTextStyle.copyWith(
                        fontSize: 20,
                        fontWeight: semiBold
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
