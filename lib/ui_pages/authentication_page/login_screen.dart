import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:skem/layouts/custom_widget/page_layout/blue_linear_background.dart';
import 'package:skem/layouts/custom_widget/buttons/button_login_sso.dart';
import 'package:skem/layouts/custom_widget/form/filled_round_input_custom.dart';
import 'package:skem/layouts/custom_widget/logo/logo_clasp.dart';
import 'package:skem/shared/shared_theme/colors.dart';
import 'package:skem/shared/shared_theme/fonts.dart';

import '../../layouts/custom_widget/buttons/filled_button_custom.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _emailTextController = TextEditingController(text: '');
  final TextEditingController _passwordTextController = TextEditingController(text: '');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: BlueLinearBackground(
          middleStop: 0.3,
          child: Stack(
            children: [
              // Icon Kuning Kiri Atas
              Positioned(
                top: -15,
                left: -45,

                child: SvgPicture.asset(
                  'assets/icon/svg_circle_kiri_atas.svg',
                  width: 170,
                  height: 170,
                ),
              ),

              // Icon Kuning Kanan Bawah
              Positioned(
                bottom: -25,
                right: -55,


                child: SvgPicture.asset(
                  'assets/icon/svg_circle_kanan_bawah.svg',
                  width: 220,
                  height: 220,
                ),
              ),

              // Main Page Content
              ListView(
                padding: const EdgeInsets.all(20),
                children: [
                  // Section Image
                  imageSection(context),

                  const SizedBox(height: 18),

                  // Section Form
                  formSection(context),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget formSection(BuildContext context){
    return Form(
      key: _formKey,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          // Email Input Form
          FilledRoundInputFormCustom(
            radius: 8,
            controller: _emailTextController,
            formLabel: 'Email',
            hintText: context.tr('login_screen.email_placeholder'),

            validator: (value) {
              if (value == null || value.isEmpty) {
                return context.tr('login_screen.email_warning');
              }
              return null;
            },
          ),

          const SizedBox(height: 18),

          // Password Input Form
          FilledRoundInputFormCustom(
            radius: 8,
            obscureText: true,
            controller: _passwordTextController,
            formLabel: 'Password',
            hintText: context.tr('login_screen.password_placeholder'),

            validator: (value) {
              if (value == null || value.isEmpty) {
                return context.tr('login_screen.password_placeholder');
              }
              return null;
            },
          ),

          const SizedBox(height: 12),

          // Text Lupa Sandi
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              InkWell(
                onTap: (){

                },
                child: Text(
                  context.tr('login_screen.forgot_password'),
                  textAlign: TextAlign.end,
                  style: GoogleFonts.notoSans(
                    color: redColor,
                    fontSize: 12,
                    fontWeight: medium,
                  ),
                ),
              ),

              const SizedBox(width: 6),
            ],
          ),

          const SizedBox(height: 40),

          // Button
          FilledButtonCustom(
            width: 130,
            height: 50,
            label: Text(
              context.tr('login_screen.login_button'),
              style: blackTextStyle.copyWith(
                fontSize: 15,
                fontWeight: semiBold,
              ),
            ),

            onTap: (){
              if(_formKey.currentState!.validate()){
                Navigator.pushNamedAndRemoveUntil(context, '/page-controller', (route) => false);
              }
            },
          ),

          const SizedBox(height: 25),

          // Atau
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              // Grey Line
              Container(
                width: MediaQuery.of(context).size.width * 0.38,
                height: 1,
                color: lightGreyColor,
              ),

              const SizedBox(width: 10),

              // Text Atau
              Text(
                'atau',
                style: GoogleFonts.notoSans(
                  color: darkBlueColor,
                  fontSize: 12,
                  fontWeight: medium,
                ),
              ),

              const SizedBox(width: 10),

              // Grey Line
              Container(
                width: MediaQuery.of(context).size.width * 0.38,
                height: 1,
                color: lightGreyColor,
              ),
            ],
          ),

          const SizedBox(height: 25),

          // Button SSO
          LoginSSOButton(label:context.tr('login_screen.login_sso_button')),
        ],
      ),
    );
  }

  Widget imageSection(BuildContext context){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        // Logo CLASP
        const LogoCLASP(
          width: 120,
          height: 28,
        ),

        const SizedBox(height: 28),

        // Text Heading Login
        Text(
          context.tr('login_screen.login_welcome'),
          textAlign: TextAlign.center,
          style: darkBlueTextStyle.copyWith(
            fontSize: 17,
            fontWeight: semiBold,
          ),
        ),

        const SizedBox(height: 30),

        // Icon Brand
        Container(
          width: 200,
          height: 190,

          decoration: const BoxDecoration(
            image: DecorationImage(
              fit: BoxFit.fill,
              image: AssetImage('assets/image/logo_brand.png'),
            ),
          ),
        ),
      ],
    );
  }
}
