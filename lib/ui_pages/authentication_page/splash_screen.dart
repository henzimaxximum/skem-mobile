
import 'package:carousel_slider/carousel_slider.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:skem/cubit/splash_screen/carousel_cubit.dart';
import 'package:skem/layouts/custom_widget/page_layout/blue_linear_background.dart';
import 'package:skem/shared/shared_theme/colors.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../layouts/custom_widget/buttons/filled_round_button_custom.dart';
import '../../layouts/custom_widget/logo/logo_clasp.dart';
import '../../shared/shared_theme/fonts.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenPageState();
}

class _SplashScreenPageState extends State<SplashScreen> {
  final CarouselController _carouselController = CarouselController();
  final CarouselCubit _carouselPageCubit = CarouselCubit();

  @override
  Widget build(BuildContext context) {
    List openingText = [
      context.tr('splash_screen.opening1'),
      context.tr('splash_screen.opening1'),
    ];

    return Scaffold(
      body: BlueLinearBackground(
        child: Stack(
          children: [
            // Icon Kuning Kiri Atas
            Positioned(
              top: -15,
              left: -45,

              child: SvgPicture.asset(
                'assets/icon/svg_circle_kiri_atas.svg',
                width: 170,
                height: 170,
              ),
            ),

            // Icon Kuning Kanan Bawah
            Positioned(
              bottom: -25,
              right: -55,

              child: SvgPicture.asset(
                'assets/icon/svg_circle_kanan_bawah.svg',
                width: 220,
                height: 220,
              ),
            ),

            // Page Content
            pageContent(openingText),

            // Disclaimer CLSAP
            Positioned(
              bottom: 6,
              left: 4,
              right: 4,

              child: Column(
                children: [
                  // Logo CLSAP
                  const LogoCLASP(),

                  const SizedBox(height: 10),

                  // Text CLSAP
                  Text(
                    context.tr('splash_screen.disclaimer'),
                    textAlign: TextAlign.center,
                    style: blackTextStyle.copyWith(
                      fontSize: 12,
                      fontWeight: light,
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  // Page Content : Carousel Image, Text, Button
  Widget pageContent(List text){
    return BlocBuilder<CarouselCubit, int>(
      bloc: _carouselPageCubit,
      builder: (context, state) {
        return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            // Image Carousel
            CarouselSlider(
              carouselController: _carouselController,

              options: CarouselOptions(
                height: 280,
                viewportFraction: 1,
                enableInfiniteScroll: false,
                onPageChanged: (index, reason) {
                  _carouselPageCubit.changePage(index);
                },
              ),

              items: [
                Image.asset(
                  'assets/image/logo_brand.png',
                  fit: BoxFit.fill,
                ),

                Image.asset(
                  'assets/image/logo_esdm.png',
                  fit: BoxFit.fill,
                ),
              ],
            ),

            const SizedBox(height: 20),

            // Carousel Circle
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  width: 12,
                  height: 12,
                  margin: const EdgeInsets.only(right: 10),

                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: state == 0 ? yellowColor : lightGreyColor,
                  ),
                ),
                Container(
                  width: 12,
                  height: 12,
                  margin: const EdgeInsets.only(right: 10),

                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: state == 1 ? yellowColor : lightGreyColor,
                  ),
                ),
              ],
            ),

            const SizedBox(height: 12),

            // Text Heading Splash
            Text(
              text[state],
              textAlign: TextAlign.center,
              style: GoogleFonts.notoSans(
                fontSize: 28,
                fontWeight: bold,
                color: blackColor,
              ),
            ),

            const SizedBox(height: 28),

            FilledRoundButtonCustom(
              width: 130,
              height: 50,

              label: Text(
                state == 0 ? 'Continue' : context.tr('splash_screen.login_button'),
                style: blackTextStyle.copyWith(
                  fontSize: 14,
                  fontWeight: bold,
                ),
              ),

              onTap: () {
                if(state == 0){
                  _carouselController.nextPage();
                } else {
                  Navigator.pushNamedAndRemoveUntil(context, '/login', (route) => false);
                }
              },
            )
          ],
        );
      },
    );
  }
}
