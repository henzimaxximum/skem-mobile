import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:skem/layouts/custom_widget/page_layout/blue_linear_background.dart';
import 'package:skem/layouts/custom_widget/logo/logo_clasp.dart';
import 'package:skem/shared/shared_theme/fonts.dart';

import '../../cubit/page_controller/page_controller_cubit.dart';
import '../../shared/shared_theme/colors.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final PageControllerCubit pageControllerCubit = BlocProvider.of<PageControllerCubit>(context);
    return Scaffold(
      body: SafeArea(
        child: BlueLinearBackground(
          middleStop: 0.7,
          child: ListView(
            padding: const EdgeInsets.all(20),
            children: [
              // Section Notification & Logo CLASP
              logoAndNotifSection(),

              const SizedBox(height: 20),

              // Section Profile User
              userProfileSection(pageControllerCubit, context),
            ],
          ),
        ),
      ), 
    );
  }

  Widget logoAndNotifSection(){
    return const Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        // Logo CLASP
        LogoCLASP(
          width: 115,
          height: 26,
        ),

        // Notif Bell
        Padding(
          padding: EdgeInsets.only(right: 10),
          child: Icon(
            Icons.notifications_none,
            size: 28,
          ),
        ),
      ],
    );
  }

  Widget userProfileSection(PageControllerCubit pageControllerCubit, BuildContext context){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        // Nama User & Copywrite
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // Text Nama User
              Text(
                'Hai, Nama User',
                style: blackTextStyle.copyWith(
                  fontSize: 23,
                  fontWeight: bold,
                ),
              ),
          
              const SizedBox(height: 4),
          
              // Text Copywrite
              Text(
                context.tr('home_screen.home_welcome_copywrite'),
                maxLines: 2,

                style: blackTextStyle.copyWith(
                  fontSize: 13.5,
                  fontWeight: regular,
                ),
              ),
            ],
          ),
        ),

        // User Profile Circle
        InkWell(
          onTap: (){
            pageControllerCubit.changePage(3);
          },
          
          child: Container(
            width: 55,
            height: 55,
          
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: lightBlueColor,
            ),
          
            child: const Center(
              child: Icon(
                Icons.person_outline,
                size: 30,
              ),
            ),
          ),
        )
      ],
    );
  }
}
