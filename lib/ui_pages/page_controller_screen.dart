import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:skem/cubit/page_controller/page_controller_cubit.dart';
import 'package:skem/shared/shared_theme/colors.dart';
import 'package:skem/ui_pages/akun_page/akun_screen.dart';
import 'package:skem/ui_pages/home_page/home_screen.dart';
import 'package:skem/ui_pages/inspeksi_page/inspeksi_screen.dart';
import 'package:skem/ui_pages/produk_page/produk_screen.dart';

import '../shared/shared_theme/fonts.dart';

class PageControllerScreen extends StatelessWidget {
  PageControllerScreen({super.key});

  final List<Map> _listNav = [
    {'icon': const Icon(Icons.home_outlined), 'label': 'Home'},
    {'icon': const Icon(Icons.event_note_outlined), 'label': 'Inspeksi'},
    {'icon': const Icon(Icons.phone_iphone), 'label': 'Produk'},
    {'icon': const Icon(Icons.person_outline), 'label': 'Akun'},
  ];


  final List<Widget> _listPages = [
    HomeScreen(),
    const InspeksiScreen(),
    const ProdukScreen(),
    const AkunScreen(),
  ];

  @override
  Widget build(BuildContext context) {
    final PageControllerCubit pageControllerCubit = BlocProvider.of<PageControllerCubit>(context);

    return BlocBuilder<PageControllerCubit, int>(
      bloc: pageControllerCubit,
      builder: (context, state){
        return Scaffold(
          body: _listPages.elementAt(state),

          bottomNavigationBar: SizedBox(
            height: 70,
            child: BottomNavigationBar(
              items: _listNav.map((menu) {
                return BottomNavigationBarItem(
                  icon: menu['icon'],
                  label: menu['label'],
                  backgroundColor: Colors.white
                );
              }).toList(),

              selectedFontSize: 12,
              selectedIconTheme: IconThemeData(
                color: blackColor,
              ),
              selectedLabelStyle: blackTextStyle.copyWith(
                fontWeight: bold,
              ),

              elevation: 8,
              currentIndex: state,
              type: BottomNavigationBarType.shifting,

              selectedItemColor: blackColor,
              unselectedItemColor: lightGreyColor,
              showUnselectedLabels: true,

              onTap: (index) {
                if(index != state) pageControllerCubit.changePage(index);
              },
            ),
          ),
        );
      },
    );
  }
}

/*
         // bottomNavigationBar: Material(
          //   elevation: 8,
          //   child: Container(
          //     color: Colors.white,
          //     padding: const EdgeInsets.only(top: 8, bottom: 6),
          //
          //     child: BottomNavigationBar(
          //       backgroundColor: Colors.transparent,
          //       type: BottomNavigationBarType.fixed,
          //       unselectedItemColor: lightGreyColor,
          //       selectedItemColor: blackColor,
          //       elevation: 0,
          //
          //       currentIndex: state,
          //       onTap: (value){
          //         _pageControllerCubit.changePage(value);
          //       },
          //
          //       selectedFontSize: 12,
          //       selectedIconTheme: IconThemeData(
          //         color: blackColor,
          //       ),
          //       selectedLabelStyle: blackTextStyle.copyWith(
          //         fontWeight: bold,
          //       ),
          //
          //       items: _listNav.map((menu) {
          //         return BottomNavigationBarItem(
          //           icon: menu['icon'],
          //           label: menu['label'],
          //         );
          //       }).toList(),
          //     ),
          //   ),
          // ),
 */
