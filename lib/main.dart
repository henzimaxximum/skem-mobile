import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:skem/cubit/page_controller/page_controller_cubit.dart';
import 'package:skem/shared/shared_theme/colors.dart';
import 'package:skem/ui_pages/akun_page/akun_screen.dart';
import 'package:skem/ui_pages/akun_page/language_setting_screen.dart';
import 'package:skem/ui_pages/authentication_page/login_screen.dart';
import 'package:skem/ui_pages/authentication_page/splash_screen.dart';
import 'package:flutter/services.dart';
import 'package:skem/ui_pages/home_page/home_screen.dart';
import 'package:skem/ui_pages/inspeksi_page/inspeksi_screen.dart';
import 'package:skem/ui_pages/inspeksi_page/pengondisi_udara/compliance_lainnya_udara/form_compliance_lainnya_udara.dart';
import 'package:skem/ui_pages/inspeksi_page/pengondisi_udara/compliance_lthe_udara/form_compliance_lthe_udara.dart';
import 'package:skem/ui_pages/inspeksi_page/pengondisi_udara/data_ritel_lthe_udara/form_ritel_baru_lthe.dart';
import 'package:skem/ui_pages/inspeksi_page/pengondisi_udara/data_ritel_lthe_udara/form_ritel_lama_lthe.dart';
import 'package:skem/ui_pages/inspeksi_page/pengondisi_udara/data_ritel_lokasi_udara/form_data_ritel_lokasi_udara.dart';
import 'package:skem/ui_pages/inspeksi_page/pengondisi_udara/detail_produk_udara/form_detail_produk_udara.dart';
import 'package:skem/ui_pages/inspeksi_page/pengondisi_udara/pengalaman_lthe_udara/form_pengalaman_lthe_udara.dart';
import 'package:skem/ui_pages/inspeksi_page/pengondisi_udara/compliance_lthe_udara/form_compliance_lanjut_udara.dart';
import 'package:skem/ui_pages/inspeksi_page/produk_kipas_angin/compliance_lainnya_angin/form_compliance_lainnya_angin.dart';
import 'package:skem/ui_pages/inspeksi_page/produk_kipas_angin/compliance_lthe_angin/form_compliance_lanjutan_angin.dart';
import 'package:skem/ui_pages/inspeksi_page/produk_kipas_angin/compliance_lthe_angin/form_complliance_lthe_angin.dart';
import 'package:skem/ui_pages/inspeksi_page/produk_kipas_angin/data_ritel_lokasi_angin/form_data_ritel_lokasi_angin.dart';
import 'package:skem/ui_pages/inspeksi_page/produk_kipas_angin/detail_produk_angin/form_detail_produk_angin.dart';
import 'package:skem/ui_pages/inspeksi_page/produk_kipas_angin/pemeriksaan_visual_angin/form_pemeriksaan_visual_angin.dart';
import 'package:skem/ui_pages/inspeksi_page/produk_kipas_angin/pengalaman_lthe_angin/form_pengalaman_lthe_angin.dart';
import 'package:skem/ui_pages/inspeksi_page/produk_kulkas/compliance_lainnya_kulkas/form_compliance_lainnya_kulkas.dart';
import 'package:skem/ui_pages/inspeksi_page/produk_kulkas/compliance_lthe_kulkas/form_compliance_lanjut_kulkas.dart';
import 'package:skem/ui_pages/inspeksi_page/produk_kulkas/compliance_lthe_kulkas/form_compliance_lthe_kulkas.dart';
import 'package:skem/ui_pages/inspeksi_page/produk_kulkas/data_ritel_lokasi_kulkas/form_data_ritel_lokasi_kulkas_screen.dart';
import 'package:skem/ui_pages/inspeksi_page/produk_kulkas/detail_produk_kulkas/form_detail_produk_kulkas.dart';
import 'package:skem/ui_pages/inspeksi_page/produk_kulkas/pemeriksaan_visual_kulkas/form_pemeriksaan_visual_kulkas.dart';
import 'package:skem/ui_pages/inspeksi_page/produk_kulkas/pengalaman_lthe_kulkas/form_pengalaman_lthe_kulkas.dart';
import 'package:skem/ui_pages/inspeksi_page/produk_lampu/compliance_lainnya_lampu/form_compliance_lainnya_lampu.dart';
import 'package:skem/ui_pages/inspeksi_page/produk_lampu/compliance_lthe_lampu/form_compliance_lanjut_lampu.dart';
import 'package:skem/ui_pages/inspeksi_page/produk_lampu/compliance_lthe_lampu/form_compliance_lthe_lampu.dart';
import 'package:skem/ui_pages/inspeksi_page/produk_lampu/data_ritel_lokasi_lampu/form_data_ritel_lokasi_lampu.dart';
import 'package:skem/ui_pages/inspeksi_page/produk_lampu/detail_produk_lampu/form_detail_produk_lampu.dart';
import 'package:skem/ui_pages/inspeksi_page/produk_lampu/pemeriksaan_visual_lampu/form_pemeriksaan_visual_lampu.dart';
import 'package:skem/ui_pages/inspeksi_page/produk_lampu/pengalaman_lthe_lampu/form_pengalaman_lthe_lampu.dart';
import 'package:skem/ui_pages/inspeksi_page/produk_penanak_nasi/compliance_lainnya_nasi/form_compliance_lainnya_nasi.dart';
import 'package:skem/ui_pages/inspeksi_page/produk_penanak_nasi/compliance_lthe_nasi/form_compliance_lanjut_nasi.dart';
import 'package:skem/ui_pages/inspeksi_page/produk_penanak_nasi/compliance_lthe_nasi/form_compliance_lthe_nasi.dart';
import 'package:skem/ui_pages/inspeksi_page/produk_penanak_nasi/data_ritel_lokasi_nasi/form_data_ritel_lokasi_nasi.dart';
import 'package:skem/ui_pages/inspeksi_page/produk_penanak_nasi/detail_produk_nasi/form_detail_produk_nasi.dart';
import 'package:skem/ui_pages/inspeksi_page/produk_penanak_nasi/pemeriksaan_visual_nasi/form_pemeriksaan_visual_nasi.dart';
import 'package:skem/ui_pages/inspeksi_page/produk_penanak_nasi/pengalaman_lthe_nasi/form_pengalaman_lthe_nasi.dart';
import 'package:skem/ui_pages/page_controller_screen.dart';
import 'package:skem/ui_pages/produk_page/produk_screen.dart';
import 'package:skem/ui_pages/sampel_uji_page/form_2/cek_fisik_form_2/cek_fisik_indoor_2_screen.dart';
import 'package:skem/ui_pages/sampel_uji_page/form_2/cek_fisik_form_2/cek_fisik_outdoor_2_screen.dart';
import 'package:skem/ui_pages/sampel_uji_page/form_2/detail_produk_form_2/form_detail_produk_2_screen.dart';
import 'package:skem/ui_pages/sampel_uji_page/form_2/hasil_final_pra_pengujian_form_2/hasil_final_pra_pengujian_2_screen.dart';
import 'package:skem/ui_pages/sampel_uji_page/form_2/informasi_umum_form_2/form_informasi_umum_2_screen.dart';
import 'package:skem/ui_pages/sampel_uji_page/form_2/pemeriksaan_visual_form_2/form_pemeriksaan_visual_2.screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  await EasyLocalization.ensureInitialized();

  runApp(
    EasyLocalization(
      path: 'assets/translations',
      startLocale: const Locale('id'),
      supportedLocales: const [
        Locale('id'),
        Locale('en', 'US'),
      ],
      child: const MyApp(),
    )
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (context) => PageControllerCubit()),
      ],

      child: MaterialApp(
        localizationsDelegates: context.localizationDelegates,
        supportedLocales: context.supportedLocales,
        debugShowCheckedModeBanner: false,
        locale: context.locale,
        title: 'Skem Mobile',
        theme: ThemeData(
          useMaterial3: false,
          scaffoldBackgroundColor: whiteBackgroundColor,
        ),

        initialRoute: '/',
        routes: {
          // Authentication Page
          '/' : (context) => const SplashScreen(),
          '/login' : (context) => const LoginScreen(),

          // App Main Navigation Page
          '/home' : (context) => const HomeScreen(),
          '/inspeksi' : (context) => const InspeksiScreen(),
          '/produk' : (context) => const ProdukScreen(),
          '/akun' : (context) => const AkunScreen(),

          // Akun Screen Sub Page
          '/page-controller' : (context) => PageControllerScreen(),
          '/language-setting' : (context) => const LanguageSettingScreen(),

          // Inspeksi Screen Sub Page (Udara)
          '/form-ritel-udara' : (context) => const FormDataRitelLokasiUdaraScreen(),
          '/form-pengalaman-lthe-udara' : (context) => const FormPengalamanLTHEUdaraScreen(),
          '/form-detail-produk-udara' : (context) => const FormDetailProdukUdaraScreen(),
          '/form-compliance-lthe-udara' : (context) => const FormComlplianceLTHEUdaraScreen(),
          '/form-compliance-lanjut-udara' : (context) => const FormComplianceLanjutUdaraScreen(),
          '/form-ritel-lama-lthe-udara' : (context) => const FormRitelLamaLTHEScreen(),
          '/form-ritel-baru-lthe-udara' : (context) => const FormRitelBaruLTHEScreen(),
          '/form-compliance-lainnya-udara' : (context) => const FormComplianceLainnyaScreen(),

          // Inspeksi Screen Sub Page (Lampu)
          '/form-ritel-lampu' : (context) => const FormDataRitelLokasiLampuScreen(),
          '/form-pengalaman-lthe-lampu' : (context) => const FormPengalamanLTHELampuScreen(),
          '/form-detail-produk-lampu' : (context) => const FormDetailProdukLampuScreen(),
          '/form-compliance-lthe-lampu' : (context) => const FormComplianceLTHELampu(),
          '/form-compliance-lanjut-lampu' : (context) => const FormComplianceLanjutLampu(),
          '/form-pemeriksaan-visual-lampu' : (context) => const FormPemeriksaanVisualLampuScreen(),
          '/form-compliance-lainnya-lampu' : (context) => const FormComplianceLainnyaLampuScreen(),

          // Inspeksi Screen Sub Page (Kulkas)
          '/form-ritel-kulkas' : (context) => const FormDataRitelLokasiKulkasScreen(),
          '/form-pengalaman-lthe-kulkas' : (context) => const FormPengalamanLTHEKulkasScreen(),
          '/form-detail-produk-kulkas' : (context) => const FormDetailProdukKulkasScreen(),
          '/form-compliance-lthe-kulkas' : (context) => const FormComplianceLTHEKulkasScreen(),
          '/form-compliance-lanjut-kulkas' : (context) => const FormComplianceLanjutKulkasScreen(),
          '/form-pemeriksaan-visual-kulkas' : (context) => const FormPemeriksaanVisualKulkasScreen(),
          '/form-compliance-lainnya-kulkas' : (context) => const FormComplianceLainnyaKulkasScreen(),

          // Inspeksi Screen Sub Page (Kipas Angin)
          '/form-ritel-angin' : (context) => const FormDataRitelLokasiAnginScreen(),
          '/form-pengalaman-lthe-angin' : (context) => const FormPengalamanLTHEAnginScreen(),
          '/form-detail-produk-angin' : (context) => const FormDetailProdukAnginScreen(),
          '/form-compliance-lthe-angin' : (context) => const FormComplianceLTHEAnginScreen(),
          '/form-compliance-lanjut-angin' : (context) => const FormComplianceLanjutanAngin(),
          '/form-pemeriksaan-visual-angin' : (context) => const FormPemeriksaanVisualAngin(),
          '/form-compliance-lainnya-angin' : (context) => const FormComplianceLainnyaAngin(),

          // Inspeksi Screen Sub Page (Nasi)
          '/form-ritel-nasi' : (context) => const FormDataRitelLokasiNasiScreen(),
          '/form-pengalaman-lthe-nasi' : (context) => const FormPengalamanLTHENasiScreen(),
          '/form-detail-produk-nasi' : (context) => const FormDetailProdukNasiScreen(),
          '/form-compliance-lthe-nasi' : (context) => const FormComplianceLTHENasiScreen(),
          '/form-compliance-lanjut-nasi' : (context) => const FormComplianceLanjutNasiScreen(),
          '/form-pemeriksaan-visual-nasi' : (context) => const FormPemeriksaanVisualNasiScreen(),
          '/form-compliance-lainnya-nasi' : (context) => const FormComplianceLainnyaNasiScreen(),

          // Pemeriksaan Sampel Uji Screen Sub Page (Form 2)
          '/form-informasi-umum-2' : (context) => const FormInformasiUmum2Screen(),
          '/form-detail-produk-2' : (context) => const FormDetailProduk2Screen(),
          '/form-pemeriksaan-visual-2' : (context) => const FormPemeriksaanVisual2Screen(),
          '/form-cek-fisik-indoor-2' : (context) => const FormCekFisikIndoor2Screen(),
          '/form-cek-fisik-outdoor-2' : (context) => const FormCekFisikOutdoor2Screen(),
          '/form-hasil-final-pengujian-2' : (context) => const HasilFinalPraPengujian2Screen(),
        },
      ),
    );
  }
}

