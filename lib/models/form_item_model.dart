class FormItemModel {
  final int id;
  final String text;

  FormItemModel({required this.id, required this.text});

  factory FormItemModel.fromJson(Map<String, dynamic> json) {
    return FormItemModel(
      id: json['id'],
      text: json['text'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'text': text,
    };
  }
}
