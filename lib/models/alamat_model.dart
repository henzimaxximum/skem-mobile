class AlamatModel{
  final double latitude;
  final double longitude;
  final String? address;

  AlamatModel({
    required this.latitude,
    required this.longitude,
    this.address,
  });
}