import 'package:flutter/material.dart';
import 'package:skem/shared/shared_theme/colors.dart';

import '../../../shared/shared_theme/fonts.dart';

class FilledRoundButtonCustom extends StatelessWidget {
  final double width;
  final double height;
  final Text label;
  final Function onTap;

  const FilledRoundButtonCustom({
    Key? key,
    required this.width,
    required this.height,
    required this.label,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      height: height,

      child: TextButton(
        onPressed: () => onTap(),

        style: TextButton.styleFrom(
          backgroundColor: yellowColor,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(56)
          ),
        ),

        child: label,
      ),
    );
  }
}