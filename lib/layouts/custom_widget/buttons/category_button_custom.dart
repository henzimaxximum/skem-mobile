import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:skem/shared/shared_theme/colors.dart';

class CategoryButtonCustom extends StatelessWidget {
  final String iconSrc;
  final String namaKategori;
  final Function onTap;

  const CategoryButtonCustom({super.key, required this.iconSrc, required this.namaKategori, required this.onTap});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        // Icon Kategori
        GestureDetector(
          onTap: () => onTap(),
          child: Container(
            width: 60,
            height: 60,

            decoration: BoxDecoration(
              color: lightBlueBackgroundColor,
              borderRadius: BorderRadius.circular(10),
            ),

            child: Center(
              child: Image.asset(
                iconSrc,
                width: 35,
                height: 35,
              ),
            ),
          ),
        ),

        const SizedBox(height: 8),

        // Text Kategori
        Text(
          namaKategori,
          textAlign: TextAlign.center,
          style: GoogleFonts.notoSans(
            fontSize: 12,
            fontWeight: FontWeight.w600,
            color: const Color(0xff1C274C),
          ),
        ),
      ],
    );
  }
}
