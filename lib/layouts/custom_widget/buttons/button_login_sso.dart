import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../shared/shared_theme/colors.dart';
import '../../../shared/shared_theme/fonts.dart';
import '../logo/logo_esdm.dart';

class LoginSSOButton extends StatelessWidget {
  final double? width;
  final double? height;
  final String label;

  const LoginSSOButton({super.key, this.width, this.height, required this.label});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width ?? double.infinity,
      height: height ?? 55,
      child: Container(
        decoration: BoxDecoration(
          color: blackColor,
          borderRadius: BorderRadius.circular(6),
        ),

        child: TextButton(
          onPressed: (){

          },
          style: TextButton.styleFrom(
            backgroundColor: blackColor,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(6),
            ),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              // Logo ESDM
              const LogoESDM(
                width: 25,
                height: 25,
              ),

              const SizedBox(width: 16),

              Text(
                label,
                style: GoogleFonts.poppins(
                  fontSize: 16,
                  fontWeight: semiBold,
                  color: yellowColor,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
