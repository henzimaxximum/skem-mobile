import 'package:flutter/material.dart';
import 'package:skem/shared/shared_theme/colors.dart';

class FilledButtonCustom extends StatelessWidget {
  final double width;
  final double height;
  final Text label;
  final Function onTap;
  final Color? buttonColor;

  const FilledButtonCustom({
    Key? key,
    required this.width,
    required this.height,
    required this.label,
    required this.onTap,
    this.buttonColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      height: height,

      child: TextButton(
        onPressed: () => onTap(),

        style: TextButton.styleFrom(
          backgroundColor: buttonColor ?? yellowColor,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(6)
          ),
        ),

        child: label,
      ),
    );
  }
}