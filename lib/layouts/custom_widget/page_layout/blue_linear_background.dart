import 'package:flutter/material.dart';

import '../../../shared/shared_theme/colors.dart';

class BlueLinearBackground extends StatelessWidget {
  final Widget child;
  final double? middleStop;

  const BlueLinearBackground({super.key, required this.child, this.middleStop});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          stops: [0.0, middleStop ?? 0.5, 1.0],
          colors: [whiteBackgroundColor, whiteBackgroundColor,lightBlueBackgroundColor],
        ),
      ),

      child: child,
    );
  }
}
