import 'package:flutter/material.dart';

import '../../../shared/shared_theme/fonts.dart';

class TopBarCustom extends StatelessWidget {
  final String title;
  final double? marginTop;
  final double? marginBottom;
  final Function onTap;

  const TopBarCustom({
    super.key,
    this.marginTop,
    this.marginBottom,
    required this.onTap,
    required this.title,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: marginTop ?? 0, bottom: marginBottom ?? 0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          // Icon Arrow Back
          Material(
            color: Colors.transparent,
            child: InkWell(
              onTap: () => onTap(),
              child: Icon(
                Icons.arrow_back,
                size: 30,
                color: Colors.black,
              ),
            ),
          ),

          // Page Title
          Text(
            title,
            style: blackTextStyle.copyWith(
              fontSize: 20,
              fontWeight: bold,
            ),
          ),

          const SizedBox(),
        ],
      ),
    );
  }
}
