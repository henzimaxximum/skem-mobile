import 'package:flutter/material.dart';

class LogoESDM extends StatelessWidget {
  final double? width;
  final double? height;

  const LogoESDM({super.key, this.width, this.height});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width ?? 80,
      height: height ?? 20,
      decoration: const BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.fill,
          image: AssetImage('assets/image/logo_esdm.png'),
        ),
      ),
    );
  }
}
