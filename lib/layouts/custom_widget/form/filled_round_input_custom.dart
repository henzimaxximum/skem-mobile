import 'package:flutter/material.dart';
import 'package:skem/shared/shared_theme/colors.dart';

import '../../../shared/shared_theme/fonts.dart';

class FilledRoundInputFormCustom extends StatelessWidget {
  final String formLabel;
  final bool obscureText;
  final TextEditingController? controller;
  final TextInputType? inputType;
  final String? hintText;
  final String? Function(String?)? validator;
  final double? radius;

  const FilledRoundInputFormCustom({
    Key? key,
    required this.formLabel,
    this.obscureText = false,
    this.controller,
    this.inputType,
    this.hintText,
    this.validator,
    this.radius,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // Email text
        Text(
          formLabel,
          style: blackTextStyle.copyWith(
              fontSize: 14,
              fontWeight: semiBold
          ),
        ),

        const SizedBox(height: 8),

        //Email Input
        TextFormField(
          obscureText: obscureText,
          controller: controller,
          keyboardType: inputType,
          validator: validator,

          decoration: InputDecoration(
            contentPadding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
            filled: true,
            fillColor: Colors.white,
            hintText: hintText,
            hintStyle: greyTextStyle.copyWith(
              fontSize: 14,
              fontWeight: regular,
            ),

            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(radius ?? 16),
            ),

            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(radius ?? 16),
              borderSide: BorderSide(color: darkYellowColor, width: 1.5),
            ),

            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(radius ?? 16),
              borderSide: BorderSide(color: Colors.transparent, width: 0),
            ),
          ),
        ),
      ],
    );
  }
}
