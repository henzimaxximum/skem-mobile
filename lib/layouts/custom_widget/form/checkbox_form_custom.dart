import 'package:flutter/material.dart';

import '../../../shared/shared_theme/fonts.dart';

class CheckboxFormCustom extends StatelessWidget {
  final String textLabel;
  final Widget listCheckbox;

  const CheckboxFormCustom({
    super.key,
    required this.textLabel,
    required this.listCheckbox,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // Text Label
        Text(
          textLabel,
          style: blackTextStyle.copyWith(
            fontSize: 13,
            fontWeight: semiBold,
          ),
        ),

        const SizedBox(height: 12),

        listCheckbox,
      ],
    );
  }
}

class CustomCheckbox extends StatelessWidget {
  final String value;
  final String? groupValue;
  final ValueChanged<String?> onChanged;

  const CustomCheckbox({
    required this.value,
    required this.groupValue,
    required this.onChanged,
  });

  @override
  Widget build(BuildContext context) {
    bool isSelected = value == groupValue;
    return GestureDetector(
      onTap: () => onChanged(value),
      child: Row(
        children: [
          // Checkbox
          Container(
            width: 20,
            height: 20,

            decoration: BoxDecoration(
              color: isSelected ? Colors.blue : Colors.transparent,
              border: Border.all(
                color: isSelected ? Colors.blue : Colors.black,
              ),
              borderRadius: BorderRadius.circular(4),
            ),

            child: isSelected
                ? const Icon(Icons.check, size: 16, color: Colors.white)
                : null,
          ),

          const SizedBox(width: 8),

          // Text Checkbox
          Text(
            value,
            style: blackTextStyle.copyWith(
              fontSize: 13,
              fontWeight: light,
            ),
          ),
        ],
      ),
    );
  }
}