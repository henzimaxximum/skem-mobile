import 'dart:io';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../shared/shared_theme/fonts.dart';
import '../../../shared/shared_values/image_icon_variable.dart';
import '../buttons/filled_button_custom.dart';

class FotoLokasiFormCustom extends StatelessWidget {
  final String? imgSrc;
  final Function onTap;

  const FotoLokasiFormCustom({super.key, this.imgSrc, required this.onTap});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.all(16),

      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadiusDirectional.circular(10),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.1),
            offset: const Offset(0, 4),
            blurRadius: 10,
          ),
        ],
      ),

      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // Text Tipe Lokasi Pengawasan
          Text(
            'Foto Lokasi Pengawasan',
            style: blackTextStyle.copyWith(
              fontSize: 15,
              fontWeight: semiBold,
            ),
          ),

          const SizedBox(height: 20),

          // Image Placeholder
          if(imgSrc == null)
            Center(
              child: Container(
                width: 280,
                height: 220,

                decoration: BoxDecoration(
                  color: const Color(0xffDADADA),
                  borderRadius: BorderRadius.circular(10),
                ),

                child: Center(
                  child: Image.asset(
                    photoPlaceholder,
                    width: 220,
                    height: 180,
                  ),
                ),
              ),
            ),

          if(imgSrc != null)
            Center(
              child: Container(
                width: 280,
                height: 220,

                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: FileImage(File(imgSrc!)),
                  )
                ),
              ),
            ),

          const SizedBox(height: 12),

          // Text Image Disclaimer
          Center(
            child: Text(
              'Unggah foto lokasi dengan ukuran maksimal 20 MB',
              textAlign: TextAlign.center,
              style: GoogleFonts.notoSans(
                color: Colors.red,
                fontSize: 12,
                fontWeight: regular,
              ),
            ),
          ),

          const SizedBox(height: 12),

          // Button Pilih Foto
          FilledButtonCustom(
            width: double.infinity,
            height: 50,
            buttonColor: const Color(0xff04AFD2),
            label: Text(
              'Pilih Foto',
              style: whiteTextStyle.copyWith(
                fontSize: 15,
                fontWeight: bold,
              ),
            ),
            onTap: () => onTap(),
          )
        ],
      ),
    );
  }
}
