import 'package:flutter/material.dart';

import '../../../shared/shared_theme/colors.dart';
import '../../../shared/shared_theme/fonts.dart';

class InspeksiInputCustom extends StatelessWidget {
  final String formLabel;
  final TextEditingController? controller;
  final TextInputType? inputType;
  final String? hintText;
  final String? Function(String?)? validator;
  final double? radius;

  const InspeksiInputCustom({
    super.key,
    required this.formLabel,
    this.controller,
    this.inputType,
    this.hintText,
    this.validator,
    this.radius
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // Email text
        Text(
          formLabel,
          style: blackTextStyle.copyWith(
            fontSize: 13,
            fontWeight: semiBold,
          ),
        ),

        const SizedBox(height: 10),

        //Email Input
        TextFormField(
          controller: controller,
          keyboardType: inputType,
          validator: validator,

          decoration: InputDecoration(
            contentPadding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
            filled: true,
            fillColor: Colors.white,
            hintText: hintText,

            hintStyle: greyTextStyle.copyWith(
              fontSize: 13,
              fontWeight: FontWeight.w100,
            ),

            border: UnderlineInputBorder(
              borderRadius: BorderRadius.circular(8),
              borderSide: BorderSide(
                color: Color(0xFFE0E0E0),
                width: 0.3
              ),
            ),
          ),
        ),
      ],
    );
  }
}
