import 'package:flutter/material.dart';

import '../../../shared/shared_theme/fonts.dart';
import '../buttons/filled_button_custom.dart';

class DenahLokasiFormCustom extends StatelessWidget {
  final Widget userLocationWidget;
  final Function onTap;

  const DenahLokasiFormCustom({super.key, required this.onTap, required this.userLocationWidget});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.all(16),

      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadiusDirectional.circular(10),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.1),
            offset: const Offset(0, 4),
            blurRadius: 10,
          ),
        ],
      ),

      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // Text Tipe Lokasi Pengawasan
          Text(
            'Denah Lokasi Pengawasan',
            style: blackTextStyle.copyWith(
              fontSize: 15,
              fontWeight: semiBold,
            ),
          ),

          const SizedBox(height: 20),

          userLocationWidget,

          const SizedBox(height: 12),

          // Button Pilih Lokasi
          FilledButtonCustom(
            width: double.infinity,
            height: 50,
            buttonColor: const Color(0xff04AFD2),
            label: Text(
              'Pilih Lokasi',
              style: whiteTextStyle.copyWith(
                fontSize: 15,
                fontWeight: bold,
              ),
            ),
            onTap: () => onTap(),
          )
        ],
      ),
    );
  }
}
