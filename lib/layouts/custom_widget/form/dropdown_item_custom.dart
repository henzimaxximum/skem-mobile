import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:skem/models/form_item_model.dart';
import 'package:skem/shared/shared_theme/fonts.dart';

class DropdownItemCustom extends StatelessWidget {
  final FormItemModel formItem;
  final bool isItemSelected;
  const DropdownItemCustom({super.key, required this.formItem, required this.isItemSelected});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 16),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          // Icon Check
          Icon(
            Icons.check_circle_outline_sharp,
            color: isItemSelected ? const Color(0xff0B87AC) : const Color(0xffA3A3A3),
            size: 20,
          ),

          const SizedBox(width: 12),

          // Text Item
          Flexible(
            child: Text(
              formItem.text,
              maxLines: 3,
              overflow: TextOverflow.ellipsis,
              style: GoogleFonts.poppins(
                color: isItemSelected ? const Color(0xff0B87AC) : const Color(0xffA3A3A3),
                fontSize: 13,
                fontWeight: regular,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
