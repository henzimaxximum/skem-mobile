import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../shared/shared_theme/fonts.dart';

class DropdownCustom extends StatelessWidget {
  final String? dropdownText;
  final Widget child;

  const DropdownCustom({
    super.key,
    this.dropdownText,
    required this.child,
  });

  @override
  Widget build(BuildContext context) {
    return ExpansionTile(
      tilePadding: const EdgeInsets.all(0),
      childrenPadding: const EdgeInsets.only(top: 10),

      shape: InputBorder.none,

      // Pilih Button
      title: Container(
        padding: const EdgeInsets.only(bottom: 10),
        decoration: BoxDecoration(
            border: Border(
                bottom: BorderSide(
                  color: Colors.grey.shade300,
                )
            )
        ),

        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            // Icon Pilih
            Container(
              width: 35,
              height: 35,

              decoration: BoxDecoration(
                color: const Color(0xffEBFBFE),
                borderRadius: BorderRadius.circular(4),
              ),

              child: const Center(
                child: Icon(
                  Icons.filter_list_sharp,
                  size: 20,
                  color: Color(0xff0B87AC),
                ),
              ),
            ),

            const SizedBox(width: 19),

            // Text Pilih
            Flexible(
              child: Text(
                dropdownText ?? 'Pilih',
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: blackTextStyle.copyWith(
                  fontSize: 14,
                  fontWeight: regular,
                ),
              ),
            ),
          ],
        ),
      ),

      // List Item Container
      children: [
        Container(
          width: double.infinity,
          constraints: const BoxConstraints(maxHeight: 210),
          padding: const EdgeInsets.only(left: 14, top: 14, right: 14),

          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              border: Border.all(
                color: Colors.grey.shade300,
                width: 1,
              )
          ),

          child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: child,
          ),
        )
      ],
    );
  }
}
