import 'package:flutter/material.dart';
import 'package:skem/shared/shared_theme/fonts.dart';

import '../../../shared/shared_values/image_icon_variable.dart';

class MapPlaceholderDefault extends StatelessWidget {
  const MapPlaceholderDefault({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 150,

      decoration: BoxDecoration(
          color: const Color(0xffDADADA),
          borderRadius: BorderRadius.circular(10),
          image: DecorationImage(
            fit: BoxFit.cover,
            image: AssetImage(mapPlaceholder),
          )
      ),
    );
  }
}

class MapPlaceholderLoading extends StatelessWidget {
  const MapPlaceholderLoading({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 150,

      decoration: BoxDecoration(
        color: const Color(0xffDADADA),
        borderRadius: BorderRadius.circular(10),
        image: DecorationImage(
          fit: BoxFit.cover,
          image: AssetImage(mapPlaceholder),
          colorFilter: ColorFilter.mode(
            Colors.black.withOpacity(0.5), // Black overlay with 50% opacity
            BlendMode.darken,
          ),
        ),
      ),

      child: Center(
        child: CircularProgressIndicator.adaptive(),
      ),
    );
  }
}

class MapPlaceholderSuccess extends StatelessWidget {
  final String address;
  const MapPlaceholderSuccess({super.key, required this.address});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 150,

      decoration: BoxDecoration(
        color: const Color(0xffDADADA),
        borderRadius: BorderRadius.circular(10),
        image: DecorationImage(
          fit: BoxFit.cover,
          image: AssetImage(mapPlaceholder),
          colorFilter: ColorFilter.mode(
            Colors.black.withOpacity(0.5), // Black overlay with 50% opacity
            BlendMode.darken,
          ),
        ),
      ),

      child: Center(
        child: Text(
          address,
          textAlign: TextAlign.center,
          style: blackTextStyle.copyWith(
            color: Colors.white,
            fontSize: 18,
            fontWeight: semiBold,
          ),
        ),
      ),
    );
  }
}

