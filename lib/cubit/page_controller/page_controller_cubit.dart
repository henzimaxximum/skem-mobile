import 'package:bloc/bloc.dart';

class PageControllerCubit extends Cubit<int> {
  PageControllerCubit() : super(0);

  void changePage(int pageIndex){
    emit(pageIndex);
  }
}
