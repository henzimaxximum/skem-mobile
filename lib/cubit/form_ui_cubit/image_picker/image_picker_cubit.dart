import 'package:bloc/bloc.dart';

class ImagePickerCubit extends Cubit<String?> {
  ImagePickerCubit() : super(null);

  void setImgSource(String imgSrc){
    emit(imgSrc);
  }
}
