import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:skem/models/alamat_model.dart';
import 'package:skem/shared/shared_method/location_picker.dart';

import '../../../shared/shared_method/general_methods.dart';

part 'location_picker_state.dart';

class LocationPickerCubit extends Cubit<LocationPickerState> {
  LocationPickerCubit() : super(LocationPickerInitial());

  void getCurrentLocation() async {
    emit(LocationPickerLoading());

    final AlamatModel userLocation = await getAddress();

    if(userLocation.address != null){
      String location = userLocation.address!;
      emit(LocationPickerSuccess(location));

    } else {
      String lat = userLocation.latitude.toString();
      String long = userLocation.longitude.toString();
      String location = 'Lat: ${lat}\nLong: ${long}';

      emit(LocationPickerSuccess(location));
    }

  }
}
