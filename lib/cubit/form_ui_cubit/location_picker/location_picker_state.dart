part of 'location_picker_cubit.dart';

@immutable
abstract class LocationPickerState {}

class LocationPickerInitial extends LocationPickerState {}

class LocationPickerLoading extends LocationPickerState {}

class LocationPickerSuccess extends LocationPickerState {
  final String location;

  LocationPickerSuccess(this.location);
}
