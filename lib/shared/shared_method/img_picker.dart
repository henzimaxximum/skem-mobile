import 'package:image_picker/image_picker.dart';

Future<XFile?> selectImageGallery() async {
  XFile? selectedImage = await ImagePicker().pickImage(source: ImageSource.gallery);

  return selectedImage;
}

Future<XFile?> selectImageCamera() async {
  XFile? selectedImage = await ImagePicker().pickImage(source: ImageSource.camera);

  return selectedImage;
}