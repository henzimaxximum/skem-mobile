import 'package:geolocator/geolocator.dart';
import 'package:geocoding/geocoding.dart';
import 'package:skem/models/alamat_model.dart';

import 'general_methods.dart';

Future<AlamatModel> getAddress() async {
  String? address;

  bool serviceEnabled = await Geolocator.isLocationServiceEnabled();
  bool isInternetConnected = await checkInternetConnectivity();

  if (!serviceEnabled) {
    return Future.error('Location services are disabled');
  }

  LocationPermission permission = await Geolocator.checkPermission();
  if (permission == LocationPermission.denied) {
    permission = await Geolocator.requestPermission();
    if (permission == LocationPermission.denied) {
      return Future.error('Location permission are denied');
    }
  }

  if (permission == LocationPermission.deniedForever) {
    return Future.error('Location permissions are permanently denied');
  }

  final Position position = await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);

  if(isInternetConnected)
    address = await getAddressFromCoordinates(position.latitude, position.longitude);

  final AlamatModel userLocation = AlamatModel(
    latitude: position.latitude,
    longitude: position.longitude,
    address: address,
  );

  return userLocation;
}

Future<String> getAddressFromCoordinates(double latitude, double longitude) async {
  List<Placemark> placemarks = await placemarkFromCoordinates(latitude, longitude);
  if (placemarks.isNotEmpty) {
    Placemark place = placemarks[0];
    return '${place.street}, ${place.locality}, ${place.postalCode}, ${place.country}';
  } else {
    return 'No address available';
  }
}

