// Image Variable
String mapPlaceholder = 'assets/image/maps_placeholder.png';
String photoPlaceholder = 'assets/image/img_placeholder.png';

// Icon Varible
String iconAc = 'assets/icon/icon_ac.png';
String iconLampu = 'assets/icon/icon_lampu.png';
String iconKulkas = 'assets/icon/icon_kulkas.png';
String iconKipasAngin = 'assets/icon/icon_kipas_angin.png';
String iconPenanakNasi = 'assets/icon/icon_penanak_nasi.png';