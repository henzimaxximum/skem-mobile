import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'colors.dart';

FontWeight light = FontWeight.w300;
FontWeight regular = FontWeight.w400;
FontWeight medium = FontWeight.w500;
FontWeight semiBold = FontWeight.w600;
FontWeight bold = FontWeight.w700;
FontWeight extraBold = FontWeight.w800;
FontWeight black = FontWeight.w900;

TextStyle blackTextStyle = GoogleFonts.notoSans(
  color: blackColor,
);

TextStyle darkBlueTextStyle = GoogleFonts.notoSans(
  color: darkBlueColor,
);

TextStyle whiteTextStyle = GoogleFonts.notoSans(
  color: Colors.white,
);

TextStyle greyTextStyle = GoogleFonts.notoSans(
  color: darkGreyColor,
);