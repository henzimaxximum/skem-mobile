import 'package:flutter/material.dart';

// Background Color
Color whiteBackgroundColor = const Color(0xffffffff);
Color lightBlueBackgroundColor = const Color(0xffD7F8FF);
Color lightBackgroundColor = const Color(0xffF6F8FB);
Color lightGreyBackgroundColor = const Color(0xffF1F1F9);

// Colors
Color blackColor = const Color(0xff14193F);
Color lightGreyColor = const Color(0xffA4A8AE);
Color darkGreyColor = const Color(0xff666666);
Color yellowColor = const Color(0xffFFF736);
Color darkYellowColor = const Color(0xffc5c40f);
Color darkBlueColor = const Color(0xff0F2851);
Color redColor = const Color(0xffFF0000);
Color lightBlueColor = const Color(0xffB9E3EA);

// Card Colors
Color lightBlueCardColor = const Color(0xffBCF0FB);
